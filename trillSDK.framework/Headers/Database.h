//
//  Database.h
//  trillSDK
//
//  Created by Mac Mini on 11/30/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "trillTech.h"

#define DATABASE_VERSION 2

@interface Database : NSObject{
    NSString* _sdk_token;
    NSString* _databaseName;
    NSString* _databasePath;
}

//DATABASE
@property (strong, nonatomic,readwrite) NSString *sdk_token;
@property (strong, nonatomic,readwrite) NSString *databaseName;
@property (strong, nonatomic,readwrite) NSString *databasePath;
+ (Database *) sharedInstance;

//--Initializer Functions
-(void)loadDatabase: ( void (^)(void) )blockCompletion;
-(void)loadDatabase:(NSString *)sdk_token withCompletionHandler:( void (^)(void) )blockCompletion  ;

//--
-(NSDictionary *)fetchCodeDictionary: (int)code islocation:(BOOL)isLocation ;
-(NSArray*)displayTable:(NSString*)tableName columnName:(NSString *)columnName;
-(NSDictionary*)displayTable: (NSString*)tableName preCondition:(NSString *)precondition selectfromTableNamePLUSCondition:(NSString *)condition;

-(void)updateColumnofTable:(NSString*)tableName withColumnName:(NSString*)columnName unique_id:(NSString *)unique_id andValue:(NSString *)value isInteger:(BOOL)isInteger;
-(void)updateTableResetIS_SEEN;

//--
-(void) insertInSyncTable:(NSDictionary *)dictionary;
-(BOOL) saveApiResults: (NSDictionary *)payload;
-(BOOL) deleteSyncTable;


@end

