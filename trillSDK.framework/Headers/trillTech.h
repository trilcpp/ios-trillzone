//
//  trillTech.h
//  trillSDK
//
//  Created by ShivanshJ on 11/14/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <CoreAudioKit/CoreAudioKit.h>

#import <AudioToolbox/Audiotoolbox.h>
#import <AudioToolbox/AudioQueue.h>
#import <AudioToolbox/AudioFile.h>


//-------Delegate
@protocol TransferDataCallBackDelegate <NSObject>
-(void)onToneDecrypted: (int) finalCode : (bool)isLocation;
-(void)onDataRecieved: (int) finalCode : (NSDictionary*)payload;

@end //end protocol


//-------Original class interface
@interface trillSDK : NSObject <AVAudioSessionDelegate> 
@property (nonatomic, retain) id <TransferDataCallBackDelegate> delegate;

- (void)initializeSDK:(NSString*) SDK_key:(NSString *)name_of_package completionHandler: ( void (^)(void) )complete;
- (void)infoCallBack;
- (void)SDKdestructor;

//-----Start/Stop Recording
- (void)startRecording;
- (void)stopRecording;


@end    //end interface


