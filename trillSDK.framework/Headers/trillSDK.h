//
//  trillSDK.h
//  trillSDK
//
//  Created by ShivanshJ on 11/14/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//



//
////! Project version number for trillSDK.
//FOUNDATION_EXPORT double trillSDKVersionNumber;
////! Project version string for trillSDK.
//FOUNDATION_EXPORT const unsigned char trillSDKVersionString[];

#import <trillSDK/trillTech.h>
#import <trillSDK/Database.h>

