App Information
===========

#### Wave View Controller

- Added Programatically. Normally you have to connect a viewcontroller in Interface Builder, and then inside it add a `UIview` which is of type WaveFormView. In `WaveViewController.swift` removing  @IBOutlet for `var waveformView`
- Addition in .swift file viewDidLoad:
```
waveformView = WaveformView(frame : CGRect(x: self.view.frame.size.width/4, y: self.view.frame.size.height/25 + self.view.frame.size.height/7,width : self.view.frame.size.width, height: self.view.frame.size.height/7));
        self.view.addSubview(waveformView)
```
-For fadeIn and Fade Out animation. Note : `flag` changes whether the wave will Rise(1) or Die(0) Down
```
//-----Comes from appDelegate
    func ChangeSwiftFlag(tflag: NSInteger , completionHandler: @escaping ()-> Void) {
        flag = tflag;
        if(tflag == 1) {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                self.waveformView.alpha = 1.0
                }, completion: {
                        (value: Bool) in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            flag = 0; //To make the way die down
                        }
                        UIView.animate(withDuration: 1.0, delay: 1.8, options: .curveEaseOut, animations: {
                            self.waveformView.alpha = 0.0
                        }, completion: {
                            (value:Bool) in
                            completionHandler() })
                            //end of completion block2
                    })//end of completion block1
        }//IF @end
    }
```
-In AppDelegate.m , to make sure WaveView is present on all screens :
```
-(void) initializeWaveViewController {
    UIViewController *current_vc = self.window.rootViewController; //cURRENT view
    UIView *temp_view = [[UIView alloc] initWithFrame:CGRectMake(current_vc.view.frame.origin.x- current_vc.view.frame.size.width/4, current_vc.view.frame.origin.y, current_vc.view.frame.size.width/10, current_vc.view.frame.size.height/10)];
    //---Swift class
    self.wVC = [[WaveViewController alloc] init];
    [self.wVC ChangeSwiftFlagWithTflag:0 completionHandler:^{}];
    //--
    [current_vc addChildViewController:self.wVC]; //Add child viewcontroller
    [current_vc.view addSubview:self.wVC.view];   //Add child's view as subview
    self.wVC.view.frame = temp_view.frame;        //Add child's frame property
    [self.wVC didMoveToParentViewController:current_vc]; //Passing the reference to the parentcontroller.
}
```

#### CAPSPageMenuViewController

- Like top tab screen for Android
- Defined programatically in a new ViewController in storyboard, like in `CouponView.m`

####  SWRevealViewController

- For hamburgerMenu

