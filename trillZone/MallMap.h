//
//  MallMap.h
//  trillZone
//
//  Created by Shivansh on 2/9/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenuViewController.h"
#import "Utilities.h"

@interface MallMap : UIViewController

@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic) CAPSPageMenu *pageMenu;

@end
