//
//  AppDelegate.h
//  trillZone
//
//  Created by Shivansh on 10/27/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <GooglePlaces/GooglePlaces.h>

#import <UserNotifications/UserNotifications.h>
@import Firebase;
#import <CoreLocation/CoreLocation.h>

#import <sys/utsname.h> //For fetching device details
#import <AdSupport/ASIdentifierManager.h>

#import "splashScreen.h"
#import "Utilities.h"
#import "ImageFetcher.h"
#import "ReachOut.h"
#import <trillSDK/Database.h>
#import <AudioToolbox/AudioServices.h>

#import "trillZone-Swift.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate,UNUserNotificationCenterDelegate, FIRMessagingDelegate, CLLocationManagerDelegate, TransferDataCallBackDelegate, FBSDKLoginButtonDelegate>

@property (strong, nonatomic) UIWindow *window;
//..All over app usage
@property (strong, nonatomic) NSString *adId;
@property (strong, nonatomic) NSString *device_model;

@property (strong, nonatomic) trillSDK *tSDK;
@property (strong, nonatomic) Database *DB;
@property (assign, nonatomic) BOOL progressBarShow; //Used in HomeController

//FUNCTIONS
-(NSString*) device_name;   //...........fetches device details
-(void)sendFCMtoServer:(NSString *)usertoken;

//swift waveViewController
@property (nonatomic, strong) WaveViewController *wVC;
-(void)ChangeFlagSwift:(NSInteger) flag;
-(void) initializeWaveViewController ;  //initialized in viewDidLoad of HomeController.

//CoreData (for setting preferences)
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
- (NSURL *)applicationDocumentsDirectory;

@end

