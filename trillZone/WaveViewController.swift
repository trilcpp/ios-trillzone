//
//  WaveViewController.swift
//  trillZone
//
//  Created by Shivansh on 2/11/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

import UIKit
import AVFoundation
import WaveformView

var flag : NSInteger = 0
var ct : Double = 0.0

@objc class WaveViewController: UIViewController {
    var audioRecorder: AVAudioRecorder!
    
    
    var waveformView: WaveformView!
    
    //override func viewAppear(_ animated: Bool) {
    override func viewDidLoad(){
        //super.viewWillAppear(animated)
        super.viewDidLoad()
        
        waveformView = WaveformView(frame : CGRect(x: self.view.frame.size.width/4, y: self.view.frame.size.height/25 + self.view.frame.size.height/7,width : self.view.frame.size.width, height: self.view.frame.size.height/7));
        self.view.addSubview(waveformView)
        
        waveformView.backgroundColor = UIColor.clear
        waveformView.alpha = 0.0
        waveformView.waveColor = hexStringToUIColor(hex:"#1b9fc6")//3498DB
        
        //audioRecorder = audioRecorder(URL(fileURLWithPath:"/dev/null"))
        //audioRecorder.record()
        
        let displayLink = CADisplayLink(target: self, selector: #selector(updateMeters))
        displayLink.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
    }
    
    
    @objc func updateMeters() {
        //audioRecorder.updateMeters()
        //NSLog("\n updateMeters()")
        let normalizedValue : Double
        if flag == 1 {
            if ct < 1.2 {
                ct = ct + 0.08
            }
            else {
                
            }
            let raise = CGFloat(-1.4 + ct) //- CGFloat(audioRecorder.averagePower(forChannel: 0)/30)
            normalizedValue = Double( pow(10, raise) )
        }
        else {
            if ct > 0 {
                ct = ct - 0.015
            }
            normalizedValue = pow(10, -1.4 + ct)
        }
        waveformView.updateWithLevel(CGFloat(normalizedValue))
    }
    
    func audioRecorder(_ filePath: URL) -> AVAudioRecorder {
        let recorderSettings: [String : AnyObject] = [
            AVSampleRateKey: 44100.0 as AnyObject,
            AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC),
            AVNumberOfChannelsKey: 1 as AnyObject,
            AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue as AnyObject
        ]
        
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
        let audioRecorder = try! AVAudioRecorder(url: filePath, settings: recorderSettings)
        audioRecorder.isMeteringEnabled = true
        audioRecorder.prepareToRecord()
        
        return audioRecorder
    }
    
    //-----Comes from appDelegate
    func ChangeSwiftFlag(tflag: NSInteger , completionHandler: @escaping ()-> Void) {
        flag = tflag;
        if(tflag == 1) {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                self.waveformView.alpha = 1.0
                }, completion: {
                        (value: Bool) in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            flag = 0; //To make the way die down
                        }
                        UIView.animate(withDuration: 1.0, delay: 1.8, options: .curveEaseOut, animations: {
                            self.waveformView.alpha = 0.0
                        }, completion: {
                            (value:Bool) in
                            completionHandler() })
                            //end of completion block2
                    })//end of completion block1
        }//IF @end
    }
    
    
    //Testers
    func hell() {
    }
    //Stackoverflow for hex to uicolour
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}

