//
//  UnlockedRecentlyView.m
//  trillZone
//
//  Created by Shivansh on 2/14/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "UnlockedRecentlyView.h"

@interface UnlockedRecentlyView ()

@end

@implementation UnlockedRecentlyView

@synthesize appDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    //----EEnabling navigation bar colour (when translucent = YES)
    [[self navigationController] navigationBar].barTintColor = [Utilities colorWithHexString:black_one];
    [[self navigationController] navigationBar].tintColor = [Utilities colorWithHexString:secondary];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *condition = @"WHERE is_seen=1 ORDER BY name ASC";
    NSDictionary *tableData = [[appDelegate DB] displayTable:@"coupon_info" preCondition:nil selectfromTableNamePLUSCondition:condition];
    NSMutableArray *unique_id = [[NSMutableArray alloc] init];
    NSMutableArray *titles = [[NSMutableArray alloc] init];
    NSMutableArray *image_URLs = [[NSMutableArray alloc] init];
    
    if([tableData count]==0){
        [Utilities showAlertWithTitle:@"No unlocks" andMessage:@"No items currently unlocked"];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    for(int i=0; i<[tableData count]; i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        [unique_id addObject:[dict valueForKey:@"id"]];
        [titles addObject:[dict valueForKey:@"brand_name"]];
        [image_URLs addObject:[dict valueForKey:@"image"]];
    }
    CGFloat leaveTopHeight = self.navigationController.navigationBar.frame.size.height + 10;
    CGFloat yPOS = 20 ;
    CGFloat scrollViewContentSize = yPOS + leaveTopHeight + 50;//+ 50 for height of the TabMenuBar
    
    NSDictionary *parameters = @{ BOX_id: unique_id,
                                  BOX_type: COUPON,
                                  
                                  BOX_titles: titles,
                                  BOX_imageURL: image_URLs,
                                  BOX_titleOpacity: @(1.0),
                                  BOX_yPOS: @(yPOS),
                                  BOX_scrollViewContentSize:@(scrollViewContentSize)
                                  };
    
    self.view.backgroundColor = [Utilities colorWithHexString:black_one];
    UIScrollView *temp_categoryScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];//ScrollView
    [Utilities makeSquareBoxes:parameters withScrollView:temp_categoryScrollView andViewController:self];
    [self.view addSubview:temp_categoryScrollView]; //---ScrollView addition
    
    //---Navigation Bar props
//    self.navigationController.navigationBar.tintColor = [Utilities colorWithHexString:tertiary];
//    UIView *solidcolor = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, leaveTopHeight)];
//    solidcolor.backgroundColor = [Utilities colorWithHexString:@"1e1e1e"];
//    [self.view addSubview:solidcolor];
    
}


- (void) viewWillAppear:(BOOL)animated{
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
