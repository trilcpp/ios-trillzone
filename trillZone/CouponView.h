//
//  CouponView.h
//  trillZone
//
//  Created by Shivansh on 2/2/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "AppDelegate.h"
#import "Utilities.h"
#import "CAPSPageMenuViewController.h"

@interface CouponView : UIViewController

@property (nonatomic) CAPSPageMenu *pageMenu;
@property (nonatomic, strong) AppDelegate *appDelegate;

@end

