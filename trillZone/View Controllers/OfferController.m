//
//  OfferController.m
//  trillZone
//
//  Created by Shivansh on 2/2/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "OfferController.h"

@interface OfferController ()
{
    NSMutableDictionary *tableData;
}
@end



@implementation OfferController

@synthesize appDelegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    tableData = [[appDelegate DB] displayTable:@"category_info" preCondition:nil selectfromTableNamePLUSCondition:@"ORDER BY category_name ASC"];
    [self loadPage];
}

-(void)viewDidAppear:(BOOL)animated{
    [self.tabBarController.tabBar setHidden:NO];
    //self.tabBarController.tabBar.translucent = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    //---NavigationBar Colour set to blue
    [[self navigationController] navigationBar].barTintColor = [Utilities colorWithHexString:black_one];
}

-(void) loadPage {
    //---Adding background colour
    self.view.backgroundColor = [Utilities colorWithHexString:black_one];

    //----Adding Background Programatically
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    imageView.image =  [UIImage imageNamed:@"background_1920"];
//    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    CGFloat yPOS = 16 ;
    //--Campaign cards
    [self campaignCards:&yPOS];
    //---Tiles
    [self scroller:&yPOS];
    
//    [self.view addSubview:imageView];
    [self.view addSubview:self.myScrollView];   //Add scrollView on Top of imageView  
}

-(void)campaignCards:(CGFloat *)yPOS{
    //-----For UIImages in scrollView
    NSArray *myTitles = @[@"",@"", @"", @"CHECKI"];
    CGFloat Titleopacity = 1.0;
    NSArray *myImages = @[@"back_no_trill",@"back_no_trill",@"back_no_trill",@"back_no_trill"];
    CGFloat imageWidth = self.view.frame.size.width;
    CGFloat imageHeight = self.view.frame.size.height*0.45;
    CGFloat scrollViewContentSize = imageWidth;                                                     //---HORIZONTAL ScrollView content size
    //self.myScrollView.contentSize = CGSizeMake(imageWidth,  self.view.frame.size.height+25);    //---ScrollView content size
    //-------Retrieval from database
    NSString *region = [[NSUserDefaults standardUserDefaults] objectForKey:@"region"];
    NSString *condition = [NSString stringWithFormat:@"WHERE LOWER(type)=LOWER('CAMPAIGN')",region];
    NSDictionary *tableData = [[appDelegate DB] displayTable:@"coupon_info" preCondition:nil selectfromTableNamePLUSCondition:condition];
    if([tableData count]==0)
        return;
    NSMutableArray *unique_id = [[NSMutableArray alloc] init];
    NSMutableArray *titles = [[NSMutableArray alloc] init];
    NSMutableArray *image_URLs = [[NSMutableArray alloc] init];
    for(int i=0; i<[tableData count]; i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        [unique_id addObject:[dict valueForKey:@"id"]];
        [titles addObject:[dict valueForKey:@"brand_name"]];
        [image_URLs addObject:[dict valueForKey:@"image"]];
    }
    NSDictionary *parameters = @{BOX_id: unique_id,
                                 BOX_type: COUPON,

                                 BOX_titles: titles,
                                 BOX_imageURL: image_URLs,
                                 BOX_titleOpacity: @(Titleopacity),
                                 BOX_imageHeight: @(imageHeight),
                                 BOX_yPOS: @(*yPOS),
                                 BOX_scrollViewContentSize:@(scrollViewContentSize)
                                 };
    [Utilities makehorizontalBoxes:parameters withScrollView:self.myScrollView andViewController:self];
    
    //-----Changing yPos since passbyReference
    (*yPOS)+=imageHeight+16;
}


-(void)scroller:(CGFloat*)yPOS {
    //-----First TWO tiles
    NSArray *myTitles = @[@"ALL",@"RECENTLY UNLOCKED",@"FAVORITES"];
    NSArray *myImages= @[@"all",@"unlock",@"favorite"];
    //NSArray *URLs = @[@"https://s3.ap-southeast-1.amazonaws.com/portaltrillbit/app_images/utility/all.png",
    //                 @"https://s3.ap-southeast-1.amazonaws.com/portaltrillbit/app_images/utility/unlock.jpg",
    //                 @"https://s3.ap-southeast-1.amazonaws.com/portaltrillbit/app_images/utility/favorite.jpg"];

    NSArray *uniqueIDs = @[@"1",@"2",@"3"];
    CGFloat Titleopacity = 1.0F;
    //----For UIImages in scrollView
    CGFloat imageHeight = self.view.frame.size.height/4;
    //CGFloat yPOS = 16 ;
    CGFloat scrollViewContentSize = *yPOS ;
    
    NSDictionary *parameters = @{BOX_type:OFFER,
                                 BOX_id: uniqueIDs,
                                 //BOX_imageURL: URLs,
                                 
                                 BOX_titles: myTitles,
                                 BOX_titleOpacity: @(Titleopacity),
                                 BOX_images: myImages,
                                 BOX_imageHeight: @(imageHeight),
                                 BOX_yPOS: @(*yPOS),
                                 BOX_scrollViewContentSize:@(scrollViewContentSize)
                                 };
    [Utilities makeRectangleBoxes:parameters withScrollView:self.myScrollView andViewController:self];
    //----Incase more rectangles to be put after this
    (*yPOS)+=(imageHeight+8)*3;
    scrollViewContentSize+=(*yPOS);
}


//----------------
#pragma mark - allCouponsViewController Segue
////----------------
-(void)allCouponViewController: (id)sender {
    CouponView *myNewVC = [[CouponView alloc] init];
//    [self presentModalViewController:myNewVC animated:YES];
    NSLog(@"%@",self);
    [self performSegueWithIdentifier:@"CouponViewSegue" sender:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
 
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
