//
//  LiveFeedController.m
//  trillZone
//
//  Created by Shivansh on 2/9/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "LiveFeedController.h"

@interface LiveFeedController ()

@end

//-------------------
#pragma mark - Landscape autorotate correction UIImagePicker
//-------------------
@implementation UIImagePickerController (NoRotate)

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate {
    return NO;
}

@end





@implementation LiveFeedController

@synthesize imageView, myScrollView ,appDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [Utilities colorWithHexString:black_one];
    [self loader];
}


//Everytime a user returns here, it rechecks everything defined here
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:NO];
    [self.tabBarController.tabBar setHidden:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    //---NavigationBar Colour set to blue
    [[self navigationController] navigationBar].barTintColor = [Utilities colorWithHexString:black_one];
}


-(void)loader{
    //---Removing things first
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    //-------ScrollView
    self.myScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:self.myScrollView];
    
    
    CGFloat yPOS=16;
    CGFloat imageWidth = self.view.frame.size.width - 16;
    CGFloat imageHeight = self.view.frame.size.height*1/4;
    //---SHARE & WIN
    UIImageView *hotTile = [[UIImageView alloc] init];
    hotTile.frame = CGRectMake(8, yPOS, imageWidth, imageHeight);
    hotTile.layer.cornerRadius = 10;
    hotTile.contentMode = UIViewContentModeCenter;
    hotTile.contentMode = UIViewContentModeScaleAspectFill;
    UIImage *img = [UIImage imageNamed:@"share_win"];
    hotTile.image = img;
    hotTile.clipsToBounds = YES;
    hotTile.layer.opacity = 0.8;
    //Shadow
    UIImageView *shadowView = [[UIImageView alloc] initWithFrame:hotTile.frame];
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
    shadowView.layer.masksToBounds = NO;
    shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    shadowView.layer.shadowOffset = CGSizeMake(1.5f, 3.0f);
    shadowView.layer.shadowOpacity = 0.8f;
    shadowView.layer.shadowPath = shadowPath.CGPath;
    shadowView.layer.shadowRadius = hotTile.layer.cornerRadius;
    [self.myScrollView addSubview:shadowView];
    [self.myScrollView addSubview:hotTile];
    //-----Text title
    UILabel *title= [[UILabel alloc] init];
    title.frame = hotTile.frame;
    title.center = hotTile.center;
    title.text = @"SHARE";
    title.font = [UIFont fontWithName:@"Bebas" size:30];
    title.textColor = [UIColor whiteColor];
    title.layer.opacity = 0.9;
    title.textAlignment = NSTextAlignmentCenter;
    title.numberOfLines = 0;
    title.minimumScaleFactor = 0.3;
    title.adjustsFontSizeToFitWidth = YES;
    title.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.myScrollView addSubview:title];
    //--aDDING Button for link
    UIButton *button = [[UIButton alloc] initWithFrame:hotTile.frame];
    [button addTarget:self action:@selector(CustomSelector:) forControlEvents:UIControlEventTouchUpInside];
    [self.myScrollView addSubview:button];

    
    //---------------Drawing Circle
    CGFloat circleSize = 60;
    UIImageView *imageView2 =  [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-circleSize-20, self.view.frame.size.height*0.65, circleSize, circleSize)];
    imageView2.contentMode = UIViewContentModeCenter;
    imageView2.contentMode = UIViewContentModeScaleAspectFit;
    imageView2.layer.cornerRadius = circleSize/2;
    imageView2.backgroundColor = [Utilities colorWithHexString:secondary];
    imageView2.clipsToBounds = NO;
    imageView2.layer.shadowColor = [UIColor blackColor].CGColor;
    imageView2.layer.shadowOffset = CGSizeMake(1, 2);
    imageView2.layer.shadowOpacity = 0.8;
    imageView2.layer.shadowRadius = 1.0;
    //-Adding Image in circle
    UIImageView *floating_image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    floating_image.center = imageView2.center;
    floating_image.image = [UIImage imageNamed:@"ic_camera_new"];
    //-Adding Button
    UIButton *camera_button = [[UIButton alloc] initWithFrame:floating_image.frame];
    //camera_button.backgroundColor = [UIColor redColor];
    [camera_button addTarget:self action:@selector(CustomSelector:) forControlEvents:UIControlEventAllEvents];
    [self.view addSubview:imageView2];
    [imageView2 setUserInteractionEnabled:YES];
    [self.view addSubview:floating_image];
    [self.view addSubview:camera_button];
    
    //----Makes a request then  draws rectangles
    [self makeGETrequest];
}


-(void) makeGETrequest{
    NSString *sdk_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"sdk_token"];
    NSString *url_string = [NSString stringWithFormat:@"%@\/live\/feeds\/",URLhit];
    NSURL *url = [NSURL URLWithString: url_string];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:sdk_token forHTTPHeaderField:@"usertoken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Utilities sendRequestToServer:request message:@"LiveFeedController/ makeGETrequest" withResponseCallback:^(NSDictionary *responseJSON) {
        NSLog(@"\n\nResponse : %@ \n...", responseJSON);
        NSDictionary *payload = [responseJSON objectForKey:@"payload"];

        //----Parsing the JSON
        NSMutableArray *unique_id = [[NSMutableArray alloc] init];
        NSMutableArray *user_names = [[NSMutableArray alloc] init];
        NSMutableArray *titles = [[NSMutableArray alloc] init];
        NSMutableArray *image_URLs = [[NSMutableArray alloc] init];
        NSMutableArray *profileImage = [[NSMutableArray alloc] init];
        NSMutableArray *is_shareable = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in payload){
            [unique_id addObject:[dict valueForKey:@"id"]];
            [user_names addObject:[dict valueForKey:@"username"]];
            [titles addObject:[dict valueForKey:@"caption"]];
            [image_URLs addObject:[dict valueForKey:@"picture"]];
            [profileImage addObject:[dict valueForKey:@"profile_pic"]];
            [is_shareable addObject:[dict valueForKey:@"is_shareable"]];
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            CGFloat Titleopacity = 1.0;
            CGFloat yPOS = self.view.frame.size.height*1/4 +60;
            CGFloat scrollViewContentSize = yPOS + self.view.frame.size.height*1/10;
            
            
            NSDictionary *parameters = @{BOX_id: unique_id, //--id
                                         user_name: user_names, //--username
                                         BOX_type: NULLVOID,
                                         @"is_shareable" : is_shareable,
                                         
                                         
                                         BOX_titles: titles,    //--Captions
                                         BOX_imageURL: image_URLs, //--Image
                                         BOX_images: profileImage, //--Profile pic
                                         
                                         BOX_titleOpacity: @(Titleopacity),
                                         BOX_yPOS: @(yPOS),
                                         BOX_scrollViewContentSize:@(scrollViewContentSize)
                                         };
            [self makeRectangleBoxes:parameters andViewController:self];
        });
    }];//@end completionHandler
}

//----------------------
#pragma mark - Make rectangle
//----------------------

- (void)makeRectangleBoxes:(NSDictionary *)options andViewController:(UIViewController *)vc{
    NSArray *myTitles = options[BOX_titles];
    CGFloat Titleopacity = [options[BOX_titleOpacity] floatValue];
    //----For UIImages in scrollView
    CGFloat imageWidth = vc.view.frame.size.width - 32;
    CGFloat imageHeight = vc.view.frame.size.height*1/4;
    CGFloat yPOS = [options[BOX_yPOS] floatValue];
    CGFloat scrollViewContentSize = [options[BOX_scrollViewContentSize]floatValue];
    
    //-Checking if someImageURLs have been sent and if BOX_id and type are being sent
    NSArray *myURLimages;
    NSArray *myImages ;
    BOOL flag_URL = NO;
    BOOL flag_exception = YES;
    for (NSString *key in options) {
        if ([key isEqualToString:BOX_imageURL]){
            flag_URL = YES;
            myURLimages = options[BOX_imageURL];
        } else if ([key isEqualToString:BOX_images]){
            myImages = options[BOX_images];
        } else if([key isEqualToString:BOX_type]){
            flag_exception = NO;
        }
    }
    
    //-Exceptions
    if(flag_exception==YES)
        [NSException raise:@"Utilities.m/Rectangle" format:@"BOX_type not in parameter list", flag_URL];
    else if([myTitles count]>[myURLimages count] && [myTitles count]>[myImages count])
        [NSException raise:@"Utilities.m/Rectangle" format:@"URLs sent=%d and titlecount > imagecount", flag_URL];
    
    NSArray *array_id = options[BOX_id];
    NSArray *user_names = options[user_name];
    NSString *type = options[BOX_type];
    NSArray *is_shareable = options[@"is_shareable"];
    
    for(int i=0; i<[myTitles count]; i++){
        //-------Add card
        UIImageView *whiteCard = [[UIImageView alloc] init];
        whiteCard.frame = CGRectMake(10, yPOS, imageWidth + 12, imageHeight + 35 + 50);
        whiteCard.backgroundColor = [Utilities colorWithHexString:black_one];
        
        CGFloat marginX = 6;
        //--Add CIRCLE DP
        CGFloat profile_image_size = 30;
        UIImageView *profile_pic = [[UIImageView alloc] init];
        profile_pic.frame = CGRectMake(marginX, 3, profile_image_size, profile_image_size);
        [profile_pic sd_setImageWithURL:[NSURL URLWithString:[myImages objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"back_no_trill"] options:SDWebImageRefreshCached];
        profile_pic.contentMode = UIViewContentModeCenter;
        profile_pic.contentMode = UIViewContentModeScaleAspectFit;
        profile_pic.layer.cornerRadius = profile_image_size/2;
        profile_pic.clipsToBounds = YES;
        [whiteCard addSubview:profile_pic];
        //--Add username
        UILabel *username = [[UILabel alloc]init];
        username.frame = CGRectMake(marginX+6 + profile_image_size, 2, imageWidth -50, profile_image_size);
        username.text = [user_names objectAtIndex:i];
        username.font = [UIFont fontWithName:@"Raleway-Bold" size:14];
        username.textColor = [UIColor whiteColor];
        [whiteCard addSubview:username];
        
        //Shadow
        whiteCard.layer.shadowColor = [UIColor blackColor].CGColor;
        whiteCard.layer.shadowOffset = CGSizeMake(1, 2);
        whiteCard.layer.shadowOpacity = 0.7;
        whiteCard.layer.shadowRadius = 1.0;
        //-------Add Tile
        UIImageView *hotTile = [[UIImageView alloc] init];
        hotTile.frame = CGRectMake(marginX, profile_image_size + 8, imageWidth, imageHeight);
        
        if(flag_URL) {
            [hotTile sd_setImageWithURL:[NSURL URLWithString:[myURLimages objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"back_no_trill"] options:SDWebImageRefreshCached];
            hotTile.contentMode = UIViewContentModeCenter;
            hotTile.contentMode = UIViewContentModeScaleAspectFill;
            hotTile.clipsToBounds = YES;
            //Shadow
            UIImageView *shadowView = [[UIImageView alloc] initWithFrame:hotTile.frame];
            UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
            shadowView.layer.masksToBounds = NO;
            shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
            shadowView.layer.shadowOffset = CGSizeMake(1.5f, 3.0f);
            shadowView.layer.shadowOpacity = 0.8f;
            shadowView.layer.shadowPath = shadowPath.CGPath;
            shadowView.layer.shadowRadius = 1.0;
            [whiteCard addSubview:shadowView];
        }
        else {
            UIImage *img = [UIImage imageNamed:[myImages objectAtIndex:i]];
            hotTile.image = img;
            hotTile.clipsToBounds = NO;
            //Shadow
            hotTile.layer.shadowColor = [UIColor blackColor].CGColor;
            hotTile.layer.shadowOffset = CGSizeMake(1, 2);
            hotTile.layer.shadowOpacity = 0.7;
            hotTile.layer.shadowRadius = 1.0;
        }
        whiteCard.layer.opacity = 0.95;
        hotTile.layer.opacity = 1.0;
        
        [whiteCard addSubview:hotTile];
        [self.myScrollView addSubview:whiteCard];
        
        //-----Add text
        UILabel *title= [[UILabel alloc] init];
        title.frame = CGRectMake(marginX, profile_image_size+hotTile.frame.size.height+ 10, hotTile.frame.size.width, 40);
        title.text = [myTitles objectAtIndex:i];
        title.font = [UIFont fontWithName:@"Raleway-Light" size:12];
        title.textColor = [UIColor whiteColor];
        title.layer.opacity = Titleopacity;

        title.textAlignment = NSTextAlignmentCenter;
        title.numberOfLines = 0;
        title.minimumScaleFactor = 0.3;
        title.adjustsFontSizeToFitWidth = YES;
        title.lineBreakMode = NSLineBreakByTruncatingTail;
        [whiteCard addSubview:title];
        
        yPOS+=whiteCard.frame.size.height+8;
        scrollViewContentSize+= whiteCard.frame.size.height+8 ;
        self.myScrollView.contentSize = CGSizeMake(imageWidth, scrollViewContentSize);
    }//end for

}


-(void)SharebuttonMethod:(UICustomButton *)button{
    //----Check if FB
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *type = [preferences objectForKey:login_type];
    if(![type isEqualToString:@"FB"]){
        [Utilities showAlertWithTitle:@"Facebook Sign In required" andMessage:@"Log Out and sign in with Facebook to Share and Win"];
        return;
    }
    
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@\/live\/feeds\/", URLhit]];
    NSString *sdk_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"sdk_token"];
    
    NSString *accessToken = [FBSDKAccessToken currentAccessToken].tokenString;
    
    NSDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[NSString stringWithFormat:@"%@", button.unique_id] forKey:@"id"];
    [dict setValue:[NSString stringWithFormat:@"%@", accessToken] forKey:@"token"];
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"PUT"];
    [request setValue:sdk_token forHTTPHeaderField:@"usertoken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:requestData];
    (@"Share to FB dictionary : %@",dict);
    [Utilities sendRequestToServer:request message:@"LiveFeed/ ShareButton" withResponseCallback:^(NSDictionary *responseJSON) {
        NSLog(@"\n\nSHARE TO FB response : %@ \n...", responseJSON);
    }];
    
    
}


//-----------------------------
#pragma mark - Image Delegates
//----------------------------
-(void)CustomSelector:(id)button{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    while ([[UIDevice currentDevice] isGeneratingDeviceOrientationNotifications])
        [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];

    [self presentViewController:picker animated:YES completion:nil];

    while ([[UIDevice currentDevice] isGeneratingDeviceOrientationNotifications])
        [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
   
    //---sET TO potrait mode
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    self.imageView = [[UIImageView alloc] init];
    self.imageView.image = chosenImage; //Then add image
    
    //    [self loader]; //Removes subviews
    [self localCard]; //then add card
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

//--------------
#pragma mark - Send To Server
-(void)sendToServer:(UICustomButton *)button {
    
    //----Check if guest
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *type = [preferences objectForKey:login_type];
    if([type isEqualToString:@"Guest"]){
        [Utilities showAlertWithTitle:@"Sign In Required" andMessage:@"Sign in to Post on LiveFeed"];
        
        //---@CloseButtonMethod : If method not successful still close
        [button.TOPview.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [button.TOPview removeFromSuperview];
        return;
    }
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@\/live\/feeds\/", URLhit]];
    NSString *sdk_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"sdk_token"];
    NSData *imageData = UIImageJPEGRepresentation(self.imageView.image, 1.0);
    
    NSMutableData *body = [NSMutableData dataWithCapacity:[imageData length] + 1024];
    //---Boundary
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //-Date
    NSDate *date_today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *stringFromDate = [dateFormat stringFromDate:date_today];
    stringFromDate = [stringFromDate stringByReplacingOccurrencesOfString:@"\ " withString:@"_"];
    stringFromDate = [stringFromDate stringByReplacingOccurrencesOfString:@"\:" withString:@"-"];
    
    NSString *test = stringFromDate;
    if (imageData)
        [body appendData:[[NSString stringWithFormat:@"%@%@%@", @"Content-Disposition: form-data; name=\"pic\"; filename=\"",test,@".jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    if (imageData)
        [body appendData:[NSData dataWithData:imageData]];

    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //-Extra Content (textview.text)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"caption\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]]; //---->Key for your parameter to send
    [body appendData:[self.textview.text dataUsingEncoding:NSUTF8StringEncoding]]; //------>Add your parameter value here
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //---End boundary
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:sdk_token forHTTPHeaderField:@"usertoken"];
//    [request setValue:imageData forHTTPHeaderField:@"image"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:body];
  
    [Utilities sendRequestToServer:request message:@"LiveFeedController/ sendToServer" withResponseCallback:^(NSDictionary *responseJSON) {
        NSLog(@"\n\nLiveFeed response : %@ \n...", responseJSON);
        //---Closing the form and reloading page, after we recieve success response
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self loader];
        }); //Dispatch mainqueue
    }];//@end completionhandler
    
    //---@CloseButtonMethod : If method not successful still close
    [button.TOPview.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [button.TOPview removeFromSuperview];
}


-(void)localCard{
    UIViewController *current_vc = self;
    
    //----Fetching Details for coupon
    NSString *unique_id;
    NSString *brand;
    NSString *name;
    NSString *descp;
    NSString *image_URL;
    int is_fav=0;
    
    brand = @"Your Message";
    
    //---Making View
    UIView *popOverCoupon = [[UIView alloc] initWithFrame:current_vc.view.frame];
    UIImageView *opaqueImage = [[UIImageView alloc] initWithFrame:popOverCoupon.frame];
    opaqueImage.layer.backgroundColor = [[UIColor blackColor]CGColor];
    opaqueImage.layer.opacity = 0.75;
    [popOverCoupon addSubview:opaqueImage];//--Add blackCover on popOverView
    
    //---CardView
    UIImageView *card = [[UIImageView alloc] initWithFrame:CGRectMake(20, popOverCoupon.frame.size.height/7, popOverCoupon.frame.size.width-40, popOverCoupon.frame.size.height*5/7)];
    card.backgroundColor = [Utilities colorWithHexString:black_one];
    //-Shadow for card
    UIImageView *shadowView = [[UIImageView alloc] initWithFrame:card.frame];
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
    shadowView.layer.masksToBounds = NO;
    shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    shadowView.layer.shadowOffset = CGSizeMake(2.0f, 3.0f);
    shadowView.layer.shadowOpacity = 0.9f;
    shadowView.layer.shadowPath = shadowPath.CGPath;
    shadowView.layer.shadowRadius = 1.0;
    [popOverCoupon addSubview:shadowView];//--Add shadow on popOverView
    //-Image on card
    UIImageView *card_image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, card.frame.size.width, card.frame.size.height/3)];
    card_image.image = self.imageView.image;
    card_image.contentMode = UIViewContentModeCenter;
    card_image.contentMode = UIViewContentModeScaleAspectFill;
    card_image.clipsToBounds = YES;
    [card addSubview:card_image];
    //-Text TITLE
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(8, card_image.frame.size.height + 8, card_image.frame.size.width - 16, card_image.frame.size.height/2)];
    title.text = brand;
    title.font = [UIFont fontWithName:@"Raleway-Bold" size:36];
    title.textColor = [Utilities colorWithHexString:@"F5AB35"];
    //        title.contentMode = UIViewContentModeCenter;
    title.minimumScaleFactor = 0.5;
    title.adjustsFontSizeToFitWidth = YES;
    [card addSubview:title];
    //-Description DESCP
    self.textview= [[UITextView alloc] initWithFrame:CGRectMake(title.frame.origin.x, card_image.frame.size.height+title.frame.size.height , title.frame.size.width, card.frame.size.height - (card_image.frame.size.height+title.frame.size.height) )];
    self.textview.delegate = self; //For the keyboard delegate
    self.textview.font = [UIFont fontWithName:@"Raleway-Regular" size:16];
    self.textview.textColor = [Utilities colorWithHexString:@"1b9fc6"];
    self.textview.backgroundColor = [UIColor whiteColor];
    self.textview.keyboardType = UIKeyboardTypeDefault;
    self.textview.font = [UIFont fontWithName:@"Raleway-Bold" size:15];
    self.textview.contentMode = UIViewContentModeTop;
    [card addSubview:self.textview];
    
    [popOverCoupon addSubview:card]; //--Add card on popOverView
    
    //---Making cross button for closing
    UIImageView *close = [[UIImageView alloc] initWithFrame:CGRectMake( card.frame.size.width - card.frame.origin.x , card.frame.origin.y-25, 50, 50)];
    close.layer.shadowColor = [[UIColor blackColor]CGColor];
    close.layer.masksToBounds = NO;
    close.layer.shadowColor = [UIColor blackColor].CGColor;
    close.layer.shadowOffset = CGSizeMake(1.5f, 3.0f);
    close.layer.shadowOpacity = 0.8f;
    close.layer.shadowRadius = 1.0;
    close.backgroundColor = [Utilities colorWithHexString:secondary];
    close.layer.opacity = 0.75f;
    close.image = [UIImage imageNamed:@"close_window"];
    [popOverCoupon addSubview:close]; //--Add close VIEW on popOverView
    //-Adding CLOSE button
    UICustomButton *close_button =[[UICustomButton alloc]initCloseButtonWithFrame:close.frame andView:popOverCoupon];
    close_button.vc = current_vc;
    //        close_button.backgroundColor = [UIColor redColor];
    [close_button addTarget:self action:@selector(closeButtonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [popOverCoupon addSubview:close_button]; //----Add close BUTTON on popOverView
    
    //END
    
    //----Adding button
        CGFloat btn_height = 50;
        //----Adding UIButton ON pOPoVERView
        CGRect button_frame = CGRectMake( card.frame.origin.x, card.frame.origin.y+card.frame.size.height-btn_height , card.frame.size.width,btn_height);
        UICustomButton *button_send = [[UICustomButton alloc] initCloseButtonWithFrame:button_frame andView:popOverCoupon];
        UIView *buttonView = [[UIView alloc] initWithFrame:button_send.frame];
        UILabel *label = [[UILabel alloc] initWithFrame:buttonView.frame];
        [buttonView addSubview:label];
        button_send.titleLabel.text = @"BUTTON";
        button_send.titleLabel.font = [UIFont fontWithName:@"Raleway-Regular" size:16];
        button_send.titleLabel.textColor = [Utilities colorWithHexString:@"1b9fc6"];
        //-Adding target of button
        [button_send addTarget:self action:@selector(sendToServer:) forControlEvents:UIControlEventTouchUpInside];
        //
        [Utilities colorDrawViewGradientLeft:@"fc4a1a" Right:@"f7b733" andSelfDotView:buttonView];
        [popOverCoupon addSubview:buttonView];
        UILabel *button_heading = [[UILabel alloc]initWithFrame:buttonView.frame];
        button_heading.text = @"Post to Feed";
        button_heading.textAlignment = NSTextAlignmentCenter;
        button_heading.font = [UIFont fontWithName:@"Raleway-Bold" size:16];
        button_heading.textColor = [UIColor whiteColor];
        [popOverCoupon addSubview:button_heading];
        [popOverCoupon addSubview:button_send];
    
    [card setUserInteractionEnabled:YES];
    [popOverCoupon setUserInteractionEnabled:YES];
    [current_vc.view addSubview:popOverCoupon];
}


-(void)closeButtonMethod:(UICustomButton *)button{
    
    [button.TOPview.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [button.TOPview removeFromSuperview];
}


#pragma mark - Keyboard mETHDods
//-----------Keyboard method
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    // Any new character added is passed in as the "text" parameter
    if ([text isEqualToString:@"\n"]) {
        // Be sure to test for equality using the "isEqualToString" message
        [textView resignFirstResponder];
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
        // Return FALSE so that the final '\n' character doesn't get added
        return FALSE;
    }
    // For any other character return TRUE so that the text gets added to the view
    return TRUE;
}


-(BOOL)textViewShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textViewEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,110,self.view.frame.size.width,self.view.frame.size.height)];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
