//
//  CinemaController.m
//  trillZone
//
//  Created by Shivansh on 2/1/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "CinemaController.h"

@interface CinemaController (){
    NSMutableArray *preferences;
}

@end

@implementation CinemaController

@synthesize appDelegate, indicator;

- (void)viewDidLoad {
    [super viewDidLoad];
    preferences = [[NSMutableArray alloc]init];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[self navigationController] navigationBar].translucent = NO;
    [[self navigationController] navigationBar].barTintColor = [Utilities colorWithHexString:bar_primary];
}

-(void) viewDidAppear:(BOOL)animated{
    [self.tabBarController.tabBar setHidden:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] navigationBar].barTintColor = [Utilities colorWithHexString:bar_primary];
    //---Adding background colour
    self.view.backgroundColor = [Utilities colorWithHexString:black_one];
    //----CORE DATA
    //Fetching Data
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Exclusive" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
    for(int i=0; i<[results count]; i++){
        NSManagedObject *device = [results objectAtIndex:i];
        NSLog(@" : %@ %@ \n", [device valueForKey:@"category_id"], [device valueForKey:@"is_marked"]);
        [preferences addObject:[device valueForKey:@"category_id"]];
    }
    [self loadPage];
}

-(void)viewDidDisappear:(BOOL)animated{
     [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
}

//-------------------------
#pragma mark - LoadPage
//-------------------------
-(void) loadPage {
    //[self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];    //Clearing everything on screen
    
    //Indicator
    self.indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.indicator.frame = CGRectMake(self.view.frame.size.width/2-40/2, self.view.frame.size.height/2-40/2- self.navigationController.navigationBar.frame.size.height, 40.0, 40.0);
    [self.view addSubview:self.indicator];
    [self.indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;

    //----Adding Background Programatically
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    imageView.image =  [UIImage imageNamed:@"background_theatre"];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    //[self.view addSubview:imageView];

    if([preferences count]==0)
        [self markPreferences];
    else{
        for(int i=0; i<[preferences count]; i++){
            NSLog(@"Preferences array : %@\n", [preferences objectAtIndex:i]);
        }
        [self tapOKPreference:nil];
    }

    //[self campaignCards:&yPOS];
    //[self scroller:&yPOS];
    //[self.view addSubview:self.myScrollView]
}


-(void)scroller:(CGFloat*)yPOS {
    //-----TB Logo
    CGFloat yAxisOfTBLogo = self.view.frame.size.height*1/25;
   
    //----For UIImages in scrollView
    CGFloat imageWidth = self.view.frame.size.width - 32;
    CGFloat imageHeight = self.view.frame.size.height*1/4;
    //CGFloat yPOS = self.view.frame.size.height*1/4 ;
    CGFloat scrollViewContentSize = *yPOS;
        UIImage *img = [UIImage imageNamed:@"extras_MOVIES"];
        UIImageView *hotTile = [[UIImageView alloc] init];
        hotTile.image = img;
        hotTile.frame = CGRectMake(16, *yPOS, imageWidth, imageHeight);
        hotTile.contentMode = UIViewContentModeCenter;
        hotTile.contentMode = UIViewContentModeScaleAspectFill;
        hotTile.clipsToBounds = YES;
        //Shadow
        UIImageView *shadowView = [[UIImageView alloc] initWithFrame:hotTile.frame];
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
        shadowView.layer.masksToBounds = NO;
        shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
        shadowView.layer.shadowOffset = CGSizeMake(1.5f, 3.0f);
        shadowView.layer.shadowOpacity = 0.8f;
        shadowView.layer.shadowPath = shadowPath.CGPath;
        shadowView.layer.shadowRadius = 1.0;
        [self.view addSubview:shadowView];
        [self.view addSubview:hotTile];
    //--aDDING Button for link
    UIButton *button = [[UIButton alloc] init];
    button.frame = CGRectMake(16, *yPOS, imageWidth, imageHeight);
    [button addTarget:self action:@selector(bookTicket:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    //-----Text BOOK TICKETS
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(0, hotTile.center.y, self.view.frame.size.width, hotTile.frame.size.height/2)];
    title.text = @"BOOK TICKETS";
    title.font = [UIFont fontWithName:@"Bebas" size:46];
    title.textColor = [UIColor whiteColor];
    title.center = hotTile.center;
    title.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:title];
}

-(void)bookTicket: (id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://gopalancinemas.com"]];
}

//-------------------------------
#pragma mark - Preferences
//-------------------------------

-(void)markPreferences{
    //-----init
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    self.view.backgroundColor = [Utilities colorWithHexString:black_one];
    [[self navigationController] navigationBar].barTintColor = [Utilities colorWithHexString:bar_primary];
    
    NSDictionary *tableData = [[appDelegate DB] displayTable:@"category_info" preCondition:nil selectfromTableNamePLUSCondition:@"ORDER BY category_name ASC"];
    //---ScrollView
    UIScrollView *newScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    newScrollView.backgroundColor = [Utilities colorWithHexString:bar_primary];
    [self.view addSubview:newScrollView];   //---------addSubview
    CGFloat newYPOS = 32;
    //----Header
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(newYPOS, 50, newScrollView.frame.size.width-64,  50)];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"Help us get to know you better.\nTell us what you are into."];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-Medium" size:20] range:NSMakeRange(0, string.length-1)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-SemiBold" size:20] range:NSMakeRange(31, string.length-31)];
    [string addAttribute:NSForegroundColorAttributeName value:[Utilities colorWithHexString:@"cfcfcf"] range:NSMakeRange(0, string.length-1)];
    [string addAttribute:NSForegroundColorAttributeName value:[Utilities colorWithHexString:secondary] range:NSMakeRange(31, string.length-31)];    //for 'offers'
    title.attributedText = string;
    title.numberOfLines = 0;
    title.adjustsFontSizeToFitWidth = YES;
    title.lineBreakMode = NSLineBreakByTruncatingTail;
    title.contentMode = UIViewContentModeCenter;
    title.textAlignment = NSTextAlignmentCenter;
    [newScrollView addSubview:title];       //-------addSubview
    
    newYPOS += title.frame.size.height+45;
    CGFloat xPOS = 8;
    CGFloat fontSize = 14;
    NSString *all_title = @"ALL";
    //-----------ALL label
    CGFloat padding = 12;
    UILabel *all_label = [[UILabel alloc]init];
    all_label.text = all_title;
    all_label.textAlignment = NSTextAlignmentCenter;
    all_label.font = [UIFont fontWithName:@"Raleway-SemiBold" size:fontSize+2];
    CGFloat newWidth = newScrollView.frame.size.width-16; //all_label.intrinsicContentSize.width+20;
    all_label.frame =CGRectMake(8, newYPOS, newWidth, fontSize+padding);
    all_label.textColor = [UIColor whiteColor];
    //-Button Label
    UICustomButton *all_button = [[UICustomButton alloc]initWithFrame:all_label.frame];
    all_button.unique_id = all_label.text;
    [all_button addTarget:self action:@selector(tapPreference:) forControlEvents:UIControlEventTouchUpInside];
    if([preferences containsObject:all_title]){
        all_button.tag = [NSNumber numberWithInt:1];
        all_button.backgroundColor = [Utilities colorWithHexString:secondary];
        [Utilities colorDrawViewGradientLeft:bar_secondary Right:secondary andSelfDotView:all_button];
    }
    else {
        all_button.tag= [NSNumber numberWithInt:0];
        all_button.backgroundColor = [UIColor darkGrayColor];
    }
    all_button.layer.cornerRadius = (all_button.frame.size.height)/2;
    all_label.clipsToBounds = YES;
    [newScrollView addSubview:all_button];      //-------addSubview
    [newScrollView addSubview:all_label];       //-------addSubview
    //-----------
    
    newYPOS += fontSize+padding+16;
    
    for(int i=0; i<[tableData count]; i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        NSString *unique_id = [dict valueForKey:@"category_id"];
        NSString *title = [dict valueForKey:@"category_name"];
        //[image_URLs addObject:[dict valueForKey:@"category_image"]];
        //--------Making Labels
        CGFloat padding = 12;
        UILabel *label = [[UILabel alloc]init];
        label.text = title;
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"Raleway-Regular" size:fontSize];
        CGFloat newWidth = label.intrinsicContentSize.width+20;
        if(newWidth+xPOS>self.view.frame.size.width){
            xPOS = 8;
            newYPOS += fontSize+padding+10;
        }
        label.frame =CGRectMake(xPOS, newYPOS, newWidth, fontSize+padding);
        label.textColor = [UIColor whiteColor];
        label.layer.cornerRadius = (label.frame.size.height)/2;
        label.clipsToBounds = YES;
        //--------Button Label
        UICustomButton *button = [[UICustomButton alloc]initWithFrame:label.frame];
        button.unique_id = title;
        [button addTarget:self action:@selector(tapPreference:) forControlEvents:UIControlEventTouchUpInside];
        if([preferences containsObject:title]){
            button.tag = [NSNumber numberWithInt:1];
            button.backgroundColor = [Utilities colorWithHexString:secondary];
        }
        else {
            button.tag= [NSNumber numberWithInt:0];
            button.backgroundColor = [UIColor darkGrayColor];
        }
        button.layer.cornerRadius = (button.frame.size.height)/2;

        [newScrollView addSubview:button];      //-------addSubview
        [newScrollView addSubview:label];       //-------addSubview
        
        xPOS += newWidth + 10;
        //newYPOS += fontSize+padding+10;
        newScrollView.contentSize = CGSizeMake(self.view.frame.size.width, newYPOS+50);
    }
    newYPOS += 55;
    //--------OK Button
    CGFloat buttonWidth = self.view.frame.size.width - 72;
    UIButton *OK_button = [[UIButton alloc] initWithFrame:CGRectMake( (self.view.frame.size.width-buttonWidth)/2, newYPOS, buttonWidth, 45)];
    [Utilities colorDrawViewGradientLeft:bar_secondary Right:secondary andSelfDotView:OK_button];//colour gradient
    [OK_button setTitle:@"SAVE" forState:UIControlStateNormal];
    OK_button.titleLabel.font = [UIFont fontWithName:@"Raleway-Bold" size:15];
    OK_button.layer.cornerRadius = (OK_button.frame.size.height)/2;
    OK_button.clipsToBounds = YES;
    [OK_button addTarget:self action:@selector(tapOKPreference:) forControlEvents:UIControlEventTouchUpInside];

    newScrollView.contentSize = CGSizeMake(self.view.frame.size.width, newYPOS+100); //----ScrollView
    [newScrollView addSubview:OK_button];       //-------addSubView
    newScrollView.userInteractionEnabled = YES;
    
    [self.view addSubview:newScrollView]; //-------addSubview
}

-(void)tapPreference:(UICustomButton*)button {
    
    //--------If all is ticked:
    if([button.unique_id isEqualToString:@"ALL"] && button.tag==[NSNumber numberWithInt:0]){//If not marked, then mark
        [preferences addObject:button.unique_id];
        
        NSDictionary *tableData = [[appDelegate DB] displayTable:@"category_info" preCondition:nil selectfromTableNamePLUSCondition:@"ORDER BY category_name ASC"];
        for(int i=0; i<[tableData count]; i++){
            NSString *key = [NSString stringWithFormat:@"%d",i];
            NSDictionary *dict = [tableData valueForKey:key];
            NSString *title = [dict valueForKey:@"category_name"];
            [preferences addObject:title];
        }
        button.tag = [NSNumber numberWithInt:1];
        button.backgroundColor = [Utilities colorWithHexString:secondary];
        [self markPreferences]; //---Reload preferences page
        return;
    } else if([button.unique_id isEqualToString:@"ALL"] && button.tag==[NSNumber numberWithInt:1]){
        [preferences removeObject:button.unique_id];
        NSDictionary *tableData = [[appDelegate DB] displayTable:@"category_info" preCondition:nil selectfromTableNamePLUSCondition:@"ORDER BY category_name ASC"];
        for(int i=0; i<[tableData count]; i++){
            NSString *key = [NSString stringWithFormat:@"%d",i];
            NSDictionary *dict = [tableData valueForKey:key];
            NSString *title = [dict valueForKey:@"category_name"];
            [preferences removeObject:title];
        }
        button.tag = [NSNumber numberWithInt:0];
        button.backgroundColor =  [UIColor darkGrayColor];
        [self markPreferences]; //---Reload preferences page
        return;
    }
    //-----@end ALL
    
    if(button.tag==[NSNumber numberWithInt:0]){ //If not marked, then mark
        button.tag = [NSNumber numberWithInt:1];
        button.backgroundColor = [Utilities colorWithHexString:secondary];
        [preferences addObject:button.unique_id];
        
        //---CORE DATA INSERT
        NSManagedObjectContext *context = [self managedObjectContext];
        NSManagedObject *transaction = [NSEntityDescription insertNewObjectForEntityForName:@"Exclusive" inManagedObjectContext:context];
        [transaction setValue:button.unique_id forKey:@"category_id"];
        [transaction setValue:[NSNumber numberWithBool:YES] forKey:@"is_marked"];
        // Save the context
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Save Failed! %@ %@", error, [error localizedDescription]);
        }
    } else if (button.tag==[NSNumber numberWithInt:1]){ //If marked, then unmark
        button.tag = [NSNumber numberWithInt:0];
        button.backgroundColor =  [UIColor darkGrayColor];
        [preferences removeObject:button.unique_id];
        
        //----CORE DATA DELETE
        NSManagedObjectContext *context = [self managedObjectContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Exclusive" inManagedObjectContext:context];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category_id = %@",button.unique_id];
        [fetchRequest setEntity:entity];
        [fetchRequest setPredicate:predicate];
        
        NSError *error;
        NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
        
        for (NSManagedObject *managedObject in items){
            [context deleteObject:managedObject];
        }
    }//@end else if
}//@end funtion

-(void)tapOKPreference:(UIButton*)sender{
    [self.indicator startAnimating];
    //----FOR PREFERENCES
    NSString *sdk_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"sdk_token"];
    NSString *url_string = [NSString stringWithFormat:@"%@\/exclusive/",URLhit];
    NSURL *url = [NSURL URLWithString: url_string];
    
    NSDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:preferences forKey:@"preference_list"];
    
    NSLog(@"\Preferences list:\n %@",dict);
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:sdk_token forHTTPHeaderField:@"usertoken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:requestData];

    [Utilities sendRequestToServer:request message:@"(Exlusive offer)CinemaController.m/ getFromServer" withResponseCallback:^(NSDictionary *responseJSON) {
        NSLog(@"\n\Preferences response : %@ \n...", responseJSON);
        [self exclusiveSendToServer];
    }];
    
}

//-------------Exclusive Tiles
#pragma mark - Exclusive Cards
-(void)exclusiveSendToServer{
    
    NSString *sdk_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"sdk_token"];
    NSString *url_string = [NSString stringWithFormat:@"%@\/exclusive/",URLhit];
    NSURL *url = [NSURL URLWithString: url_string];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:sdk_token forHTTPHeaderField:@"usertoken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Utilities sendRequestToServer:request message:@"(Exlusive offer)CinemaController.m/ getFromServer" withResponseCallback:^(NSDictionary *responseJSON) {
        NSLog(@"\n\nExclusive response : %@ \n...", responseJSON);
        dispatch_async(dispatch_get_main_queue(), ^{
            [[self navigationController] navigationBar].barTintColor = [Utilities colorWithHexString:black_one];
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            [self exclusiveCards:[responseJSON objectForKey:@"payload"]];
            [self.indicator stopAnimating];
        });
    }];
}

-(void)edit_preference:(CGFloat)rectangleHeight:(CGFloat)yPOS : (UIScrollView*)newScrollView{
    UIView *PreferenceRect = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, rectangleHeight)];
    //PreferenceRect.backgroundColor = [UIColor whiteColor];
    UIButton *clickPreferences = [[UIButton alloc]initWithFrame:PreferenceRect.frame];
    [clickPreferences setBackgroundImage:[Utilities imageWithColor:[Utilities colorWithHexString:black_two]] forState:UIControlStateHighlighted];
    //-Button Preference
    [clickPreferences addTarget:self action:@selector(markPreferences) forControlEvents:UIControlEventTouchUpInside];
    [newScrollView addSubview:PreferenceRect];   //---addSubview to scroll
    [newScrollView addSubview:clickPreferences];   //---addSubview to scroll
    
    //----Image
    CGFloat img_edit_width = rectangleHeight*0.6;
    UIImageView *img_edit = [[UIImageView alloc]initWithFrame:CGRectMake(16, (rectangleHeight-img_edit_width)/2, img_edit_width, img_edit_width)];
    img_edit.image = [UIImage imageNamed:@"ic_edit"];
    img_edit.contentMode = UIViewContentModeScaleAspectFit;
    [PreferenceRect addSubview:img_edit];           //----addSubview to EDIT rectangle
    //---Label
    CGFloat fontSize = 14;
    UILabel *label = [[UILabel alloc]init];
    label.text = @"Edit your preferences";
    label.textColor = [Utilities colorWithHexString:black_two];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Raleway-Regular" size:fontSize];
    CGFloat newWidth = label.intrinsicContentSize.width+20;
    label.frame =CGRectMake(16+img_edit_width+8, img_edit.frame.origin.y , newWidth, img_edit_width);
    [PreferenceRect addSubview:label];           //----addSubview to EDIT rectangle
    
    //-00 DrawLine
    CGRect frame = PreferenceRect.frame;
    CGPoint point = CGPointMake(CGRectGetMaxX(frame), CGRectGetMaxY(frame));
    UIView *Line = [[UIView alloc]initWithFrame:CGRectMake(0, point.y , self.view.frame.size.width, 0.3)];
    Line.backgroundColor = [Utilities colorWithHexString:black_two];
    [newScrollView addSubview:Line];            //--addSubView Line
    //-00
}

-(void)exclusiveCards:(NSDictionary*)payload{
    //-----ScrollView
    UIScrollView *newScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    newScrollView.backgroundColor = [Utilities colorWithHexString:black_one];
    
    CGFloat yPOS = 8;
    CGFloat rectangleHeight = 100  ;
    
    //----Edit Preferences option
    [self edit_preference:rectangleHeight*0.4:yPOS:newScrollView];
    
    yPOS+=(rectangleHeight*0.4)+16;
    
    for(NSDictionary *array in payload){
        NSString *unique_id = [array objectForKey:@"id"];
        NSString *code = [array objectForKey:@"code"];
        NSString *title = [array objectForKey:@"title"];
        NSString *info = [array objectForKey:@"info"];
        NSArray *description = [array objectForKey:@"descp"];
        NSString *image_URLs = [array objectForKey:@"image"];
        
        NSString *points_req = [array objectForKey:@"points_req"];
        NSNumber *is_seen = [array objectForKey:@"is_seen"];
        
        //------Making rectangular view
        CGFloat xOrigin = 16;
        UIView *rectangle = [[UIView alloc]initWithFrame:CGRectMake(xOrigin, yPOS, self.view.frame.size.width-xOrigin, rectangleHeight)];
        UICustomButton *clickPopOpen = [[UICustomButton alloc]initWithFrame:rectangle.frame];
        //---Button RECTANGLE
        clickPopOpen.unique_id = unique_id;
        clickPopOpen.type = code;
        clickPopOpen.tag = is_seen;
        clickPopOpen.array = @[description,points_req];
        [clickPopOpen addTarget:self action:@selector(popOpenExclusiveButton:) forControlEvents:UIControlEventTouchUpInside];
        //------Drawing Circle
        CGFloat circleSize = rectangle.frame.size.height*0.65;
        UIImageView *circleView = [[UIImageView alloc]initWithFrame:CGRectMake(8, (rectangle.frame.size.height-circleSize)/2, circleSize,  circleSize)];
        circleView.contentMode = UIViewContentModeCenter;
        circleView.contentMode = UIViewContentModeScaleAspectFill;
        [circleView sd_setImageWithURL:[NSURL URLWithString:image_URLs] placeholderImage:[UIImage imageNamed:@"back_no_trill"]];
        circleView.layer.cornerRadius = circleSize/2;
        circleView.clipsToBounds = YES;
        [rectangle addSubview:circleView];      //---addSubView
        //--Outline Circle
        CGFloat circleSize2 = rectangle.frame.size.height*0.8;
        UIImageView *circleView2 = [[UIImageView alloc]initWithFrame:CGRectMake(8, (rectangle.frame.size.height-circleSize2)/2, circleSize2,  circleSize2)];
        circleView2.center = circleView.center;
        circleView2.contentMode = UIViewContentModeCenter;
        circleView2.contentMode = UIViewContentModeScaleAspectFill;
        [circleView sd_setImageWithURL:[NSURL URLWithString:image_URLs] placeholderImage:[UIImage imageNamed:@"back_no_trill"]];
        circleView2.layer.cornerRadius = circleSize2/2;
        //circleView.clipsToBounds = YES;
        circleView2.layer.borderWidth = 4.0;
        circleView2.layer.borderColor = [[Utilities colorWithHexString:secondary] CGColor];
        [rectangle addSubview:circleView2];      //---addSubView
        //------Label Title
        CGFloat fontSize = 18;
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(xOrigin+circleSize2, rectangle.frame.size.height/2-circleSize/4-7, 120, fontSize)];
        titleLabel.text = title;
        titleLabel.font = [UIFont fontWithName:@"Raleway-Bold" size:fontSize];
        titleLabel.textColor = [UIColor whiteColor];
        [rectangle addSubview:titleLabel];           //---addSubview
        //-----TextView Description
        UITextView *descp = [[UITextView alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x-3, titleLabel.frame.origin.y+titleLabel.frame.size.height-3, 190, rectangle.frame.size.height/2 - 10)];
        descp.text = info;
        descp.font = [UIFont fontWithName:@"Raleway-Medium" size:fontSize-5.5];
        descp.textColor = [Utilities colorWithHexString:@"cccccc"];
        descp.backgroundColor = [UIColor clearColor];
        descp.userInteractionEnabled = NO;
        //-size
        CGFloat fixedWidth = descp.frame.size.width;
        CGSize newSize = [descp sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = descp.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        descp.frame = newFrame;
        [rectangle addSubview:descp];           //---addSubview
        //------Details Box Label
        CGFloat boxWidth = 90;
        CGFloat boxHeight = 22;
        UILabel *titlebOX = [[UILabel alloc] initWithFrame:CGRectMake(rectangle.frame.size.width-xOrigin - boxWidth-2, circleView.frame.origin.y-boxHeight/2, boxWidth, boxHeight)];
        titlebOX.text = [NSString stringWithFormat:@"%@ POINTS",points_req];
        titlebOX.textAlignment = NSTextAlignmentCenter;
        titlebOX.font = [UIFont fontWithName:@"Raleway-SemiBold" size:fontSize-6.5];
        titlebOX.textColor = [Utilities colorWithHexString:secondary];
        titlebOX.layer.borderWidth = 1.0;
        titlebOX.layer.borderColor = [[Utilities colorWithHexString:secondary]CGColor];
        [rectangle addSubview:titlebOX];           //---addSubview
        //-00 DrawLine
        CGRect frame = rectangle.frame;
        CGPoint point = CGPointMake(CGRectGetMaxX(frame), CGRectGetMaxY(frame));
        UIView *Line = [[UIView alloc]initWithFrame:CGRectMake(rectangle.frame.origin.x, point.y+16 , rectangle.frame.size.width-xOrigin, 0.6)];
        Line.backgroundColor = [Utilities colorWithHexString:black_two];
        [newScrollView addSubview:Line];            //--addSubView Line
        //-00
        
        yPOS += rectangle.frame.size.height + 32;
        newScrollView.contentSize = CGSizeMake(self.view.frame.size.width, yPOS+25);
        [newScrollView addSubview:rectangle];   //---addSubview to scroll
        [newScrollView addSubview:clickPopOpen];   //---addSubview to scroll
    }
    [self.view addSubview:newScrollView];   //---------addSubview
}


-(void)popOpenExclusiveButton:(UICustomButton*)customButton {
    NSLog(@"popOpen exclusive button");
    UIViewController *current_vc = self.tabBarController;
    NSString *unique_id = customButton.unique_id;
    NSString *code = customButton.type;
    NSString *descp_redeem = [[customButton.array objectAtIndex:0] objectAtIndex:0];
    NSString *descp_terms = [[customButton.array objectAtIndex:0] objectAtIndex:1];
    NSString *points_req = [customButton.array objectAtIndex:1] ;
    NSNumber *is_seen = customButton.tag ;
    
    //---Making View
    UIView *popOverCoupon = [[UIView alloc] initWithFrame:current_vc.view.frame];
    UIImageView *opaqueImage = [[UIImageView alloc] initWithFrame:popOverCoupon.frame];
    opaqueImage.layer.backgroundColor = [[UIColor blackColor]CGColor];
    opaqueImage.layer.opacity = 0.75;
    [popOverCoupon addSubview:opaqueImage];//--Add blackCover on popOverView
    //Button to ClosePopOver touch outside bounds
    UICustomButton *close_popover =[[UICustomButton alloc] initCloseButtonWithFrame:popOverCoupon.frame andView:popOverCoupon];
    close_popover.vc = current_vc;
    [close_popover addTarget:self action:@selector(closeButtonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [popOverCoupon addSubview:close_popover]; //----Add close BUTTON on popOverView
    
    //---CardView
    CGFloat imageSize = popOverCoupon.frame.size.height*0.4;
    UIImageView *card = [[UIImageView alloc] initWithFrame:CGRectMake(20, popOverCoupon.frame.size.height/7, popOverCoupon.frame.size.width-40, imageSize)];
    card.backgroundColor = [Utilities colorWithHexString:@"ffffff"];
    card.center = popOverCoupon.center;
    UIScrollView *cardScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, card.frame.size.width-16, card.frame.size.height)];
    cardScroll.showsVerticalScrollIndicator = YES;
    CGFloat yPOS = 16;
    
    //-Text TITLE
    CGFloat fontSize = 20;
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(16, 16, cardScroll.frame.size.width - 16, fontSize+5)];
    title.text = @"How to redeem?";
    title.font = [UIFont fontWithName:@"Raleway-Bold" size:fontSize];
    title.textColor = [Utilities colorWithHexString:primary];
    title.minimumScaleFactor = 0.5;
    title.adjustsFontSizeToFitWidth = YES;
    [cardScroll addSubview:title];
    //***********Description Dynamic SIZE
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(title.frame.origin.x, title.frame.origin.y + fontSize, title.frame.size.width, 100.0f)];
    // add text to the UITextView first so we know what size to make it
    textView.text = descp_redeem;
    textView.font = [UIFont fontWithName:@"Raleway-Medium" size:14];
    textView.textColor = [Utilities colorWithHexString:primary];
    textView.backgroundColor = [UIColor clearColor];
    // get the size of the UITextView based on what it would be with the text
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    [textView setUserInteractionEnabled:NO]; //To disable editing of textView
    [cardScroll addSubview:textView];
    //***********
    
    CGFloat gap=20;
    //-Text TITLE--2
    UILabel *title2= [[UILabel alloc] initWithFrame:CGRectMake(title.frame.origin.x, textView.frame.origin.y + newSize.height +gap, cardScroll.frame.size.width - title.frame.origin.x, fontSize+5)];
    title2.text = @"Terms & Conditions";
    title2.font = [UIFont fontWithName:@"Raleway-Bold" size:fontSize];
    title2.textColor = [Utilities colorWithHexString:bar_primary];
    title2.minimumScaleFactor = 0.5;
    title2.adjustsFontSizeToFitWidth = YES;
    [cardScroll addSubview:title2];
    //***********Description Dynamic SIZE
    UITextView *textView2 = [[UITextView alloc] initWithFrame:CGRectMake(title2.frame.origin.x, title2.frame.origin.y + fontSize, title2.frame.size.width, 100.0f)];
    // add text to the UITextView first so we know what size to make it
    textView2.text = descp_terms;
    textView2.font = [UIFont fontWithName:@"Raleway-Medium" size:14];
    textView2.textColor = [Utilities colorWithHexString:primary];
    textView2.backgroundColor = [UIColor clearColor];
    // get the size of the UITextView based on what it would be with the text
    CGFloat fixedWidth2 = textView2.frame.size.width;
    CGSize newSize2 = [textView2 sizeThatFits:CGSizeMake(fixedWidth2, MAXFLOAT)];
    CGRect newFrame2 = textView2.frame;
    newFrame2.size = CGSizeMake(fmaxf(newSize2.width, fixedWidth2), newSize2.height);
    textView2.frame = newFrame2;
    [textView2 setUserInteractionEnabled:NO]; //To disable editing of textView
    [cardScroll addSubview:textView2];
    

    //-------Button
    CGFloat btn_height = 50;
    card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y, card.frame.size.width, card.frame.size.height + btn_height + 3);
        //-Shadow for card
        UIImageView *shadowView = [[UIImageView alloc] initWithFrame:card.frame];
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
        shadowView.layer.masksToBounds = NO;
        shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
        shadowView.layer.shadowOffset = CGSizeMake(2.0f, 3.0f);
        shadowView.layer.shadowOpacity = 0.9f;
        shadowView.layer.shadowPath = shadowPath.CGPath;
        shadowView.layer.shadowRadius = 1.0;
        [popOverCoupon addSubview:shadowView];//--Add shadow on popOverView
    //*
    yPOS += (16+title.frame.size.height+textView.frame.size.height)+gap+(title2.frame.size.height+textView2.frame.size.height);
    cardScroll.contentSize = CGSizeMake(cardScroll.frame.size.width, yPOS);
    [card addSubview:cardScroll];
    [popOverCoupon addSubview:card]; //--Add card on popOverView
    //*
        //----Adding UIButton ON pOPoVERView
        CGRect button_frame = CGRectMake( card.frame.origin.x, card.frame.origin.y+card.frame.size.height-btn_height , card.frame.size.width,btn_height);
        UICustomButton *button_send = [[UICustomButton alloc] initCloseButtonWithFrame:button_frame andView:popOverCoupon];
        button_send.tag = customButton.tag;
        //-Adding target of button
        if([button_send.tag integerValue] == 0){
            [button_send addTarget:self action:@selector(ButtonUnlockOffer:) forControlEvents:UIControlEventTouchUpInside];
            [button_send setTitle:@"UNLOCK OFFER" forState:UIControlStateNormal];
        } else [button_send setTitle:code forState:UIControlStateNormal];
        [button_send.titleLabel setFont:[UIFont fontWithName:@"Raleway-Bold" size:16.0]];
        button_send.unique_id = unique_id;
        button_send.type = code;
        button_send.array = @[points_req];
        //
        UIView *buttonView = [[UIView alloc] initWithFrame:button_send.frame];
        [Utilities colorDrawViewGradientLeft:bar_secondary Right:secondary andSelfDotView:button_send];
        [popOverCoupon addSubview:button_send];

    
    [popOverCoupon setUserInteractionEnabled:YES];
    [card setUserInteractionEnabled:YES]; //The touch events always come to the view on top, unless cardView touch is enabled.
    [current_vc.view addSubview:popOverCoupon];
}

-(void)closeButtonMethod:(UICustomButton *)button{
    [button.TOPview.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [button.TOPview removeFromSuperview];
    
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self exclusiveSendToServer];
}

-(void)ButtonUnlockOffer:(UICustomButton*)button_send{
    NSLog(@"Code unlocked");
    NSString *points_req = [button_send.array objectAtIndex:0];
    int points = [points_req integerValue];
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    int curr_trill_points = [preferences integerForKey:@"trill_points"];
    if(points > curr_trill_points){
        [Utilities showAlertWithTitle:@"Trill Points" andMessage:@"Collect more trill points to unlock this offer"];
        return;
    }
    
    button_send.tag= [NSNumber numberWithInteger:1]; //Change button tag
    
    NSString *sdk_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"sdk_token"];
    NSString *url_string = [NSString stringWithFormat:@"%@/exclusive/redeem/",URLhit];
    NSURL *url = [NSURL URLWithString: url_string];
    
    NSDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:button_send.unique_id forKey:@"offer_id"];
    
    NSLog(@"\Preferences list:\n %@",dict);
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:sdk_token forHTTPHeaderField:@"usertoken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:requestData];
    
    [Utilities sendRequestToServer:request message:@"(Exlusive offer)CinemaController.m/ ButtonUnlockOffer" withResponseCallback:^(NSDictionary *responseJSON) {
        NSLog(@"Response Json:%@",responseJSON);
        dispatch_async(dispatch_get_main_queue(),^{
            [button_send setTitle:button_send.type forState:UIControlStateNormal];
        });
        [Utilities addTrillPoints:(-points)];
    }];
    
}

//---------------------------------
#pragma mark - CoreData
//------------

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
