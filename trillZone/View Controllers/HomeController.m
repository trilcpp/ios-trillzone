//
//  ViewController.m
//  trillZone
//
//  Created by Shivansh on 10/27/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import "HomeController.h"
#import <Lottie/Lottie.h>
//#import "LTMorphingLabel-Swift.h"       // these won't be in the pod. You'll have to fetch


@interface ViewController () 
@end




@implementation ViewController{
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSMutableArray *text_array;
}

@synthesize appDelegate,label, hotTile, rippleLayer;
@synthesize player,controller;
@synthesize progressView, myTimer, labelProgress;
NSString *API_BASE_URL = URLhit;

- (void)viewDidLoad {
    [super viewDidLoad];
    text_array = @[@"SEARCHING BY SOUND",@"SENSING YOUR ENVIRONMENT",@"FINDING BEST OFFERS FOR YOU",@"TAP TO STOP SENSING"];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[appDelegate tSDK] startRecording];
    
    self.controller = [[AVPlayerViewController alloc] init];
    
    //----Init WAVEViewController (doing it here instead of AppDelegate incase person logsOut and logsBackIn)
    [appDelegate initializeWaveViewController];
    self.navigationController.navigationBar.translucent = NO;
    
    // Add an observer - Region change
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationRegionChange:) name:@"UserDefaultRegion" object:nil];
    // Add an observer - Application Enters Foreground
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationRegionChange:) name:@"ForegroundHome" object:nil];

    //---Adding background colour
    self.view.backgroundColor = [Utilities colorWithHexString:primary];
    //-----API URLs
    [self apiCalls];
    
    //----For location initialization
    geocoder = [[CLGeocoder alloc] init];
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    self.locationManager.delegate = self;
    
    //----GeoFencing,
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    //---Check for location permissions
    if ([CLLocationManager locationServicesEnabled]){
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            [Utilities showAlertWithTitle:@"Improve your Experience" andMessage:[NSString stringWithFormat:@"To re-enable Location Services, go to Settings and turn on Location Service for this app."]];
        }
    }
    [self.locationManager startUpdatingLocation];
    [self geoLocation];
    
    //------Text Animation
    self.label = [[TOMSMorphingLabel alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, 42)];
    self.label.text = @"FINDING OFFERS NEAR YOU";
    self.label.animationDuration = 0.37;
    self.label.characterAnimationOffset = 0.25;
    self.label.textColor = [Utilities colorWithHexString:black_two];
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.font = [UIFont fontWithName:@"Raleway-SemiBold" size:15];
    
    [NSTimer scheduledTimerWithTimeInterval:3.0f
                                     target:self selector:@selector(animateText:) userInfo:nil repeats:YES];
    
    //-----Progress Bar
    #pragma mark trillpoints Minutes set
    initSeconds = 0;
    //NOTE : TRILL_points earned are minutes*10
    progress_array[0] = 1;  //In minutes,
    progress_array[1] = 1;  //In minutes
    progress_array[2] = 1;  //In minutes
    [self setCurrentTime:progress_array[0]:initSeconds];
}


int iterator = 0;
-(void)animateText:(NSTimer *)timer {
    //NSLog(@"Text array size: %d | iterator count: %d",[text_array count],iterator);
    iterator= (iterator+1)%[text_array count];
    [self.label setText:[text_array objectAtIndex:iterator] withCompletionBlock:^{}];
}



#pragma mark - Notification
- (void) NotificationRegionChange:(NSNotification *) notification {
    [Utilities checkCampaign:self];     //Goes to Utilities, there, viewcontroller switches to tab bar controller and displays
    //Page reloads after dialogue box appears
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5* NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self loadPage];
    });
    
}

-(void)geoLocation {
    NSMutableArray * geofences = [self mapDictionaryToRegion];
    for(CLRegion *geofence in geofences) {
        [self.locationManager startMonitoringForRegion:geofence desiredAccuracy:kCLLocationAccuracyBest];
    }
    BOOL locationAllowed = [CLLocationManager locationServicesEnabled];
    NSLog(@"\n\nLOCATION SERVICES allowed-, %d",locationAllowed);
    NSArray *MonitorAll = [[self.locationManager monitoredRegions] allObjects];
    NSLog(@"\n\nMonitored regions : Enabled-%d \ncount-%d Regions-%@\n...",[CLLocationManager regionMonitoringEnabled], [MonitorAll count], MonitorAll);
}

-(void)viewWillAppear:(BOOL)animated{
    [self.tabBarController.tabBar setHidden:NO];
    //----Navigation Bar Colour
    [[self navigationController] navigationBar].barTintColor = [Utilities colorWithHexString:primary];
}

-(void)viewDidDisappear:(BOOL)animated{
   [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
}

//Everytime a user returns here, it rechecks everything defined here
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:NO];
     //----Loading the whole page with scrollView
    [self loadPage];
}

//-------------------------------------
#pragma mark - locationManager Delegate methods
//-------------------------------------

-(void)locationManager : (CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *newLocation = [locations lastObject];
    NSLog(@"\n\n\n\n - Entered self.locationManager didUpdateLocations \n\n");
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if (error == nil&& [placemarks count] >0) {
            placemark = [placemarks lastObject];
            NSString *state, *country;
            double latitude, longitude;
            latitude = newLocation.coordinate.latitude;
            longitude = newLocation.coordinate.longitude;
            state = placemark.administrativeArea;
            country = placemark.country;
            current_zone = [ViewController getClosestZoneLocation_ZoneID: latitude : longitude];    //Here ViewController is className for HomeController
            NSLog(@"\n\n Latitude - %f, \n Longitude - %f \n\n", latitude, longitude);
        } else {
            NSLog(@" error.debugDescription %@", error.debugDescription);
        }
    }];
   
    // Turn off the location manager to save power.
    [manager stopUpdatingLocation];
}

- (NSMutableArray*)mapDictionaryToRegion
{   
    NSArray *zone_id= [[appDelegate DB] displayTable:@"zone_info" columnName:@"zone_id"];
    NSArray *zoneTableData_lat= [[appDelegate DB] displayTable:@"zone_info" columnName:@"latitude"];
    NSArray *zoneTableData_longitude= [[appDelegate DB] displayTable:@"zone_info" columnName:@"longitude"];
    NSMutableArray *geofences = [[NSMutableArray alloc] init];
    
    for(int i=0; i<[zoneTableData_lat count]; i++ ) {
        NSString *identity = [NSString stringWithFormat:@"%@", zone_id[i]];
        CLLocationDegrees latitude =  [ zoneTableData_lat[i]  doubleValue];
        CLLocationDegrees longitude = [ zoneTableData_longitude[i]  doubleValue];
        CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(latitude, longitude);
//        NSLog(@"ViewController/mapDictionaryToRegion : %f %f",latitude,longitude);
        //TODO: Radius geolocation
        CLLocationDistance regionRadius = 600;
        CLRegion *region =[[CLCircularRegion alloc] initWithCenter:centerCoordinate radius:regionRadius identifier:identity];
        [geofences addObject:region];
    }
    return geofences;
}

+(BOOL)getClosestZoneLocation_ZoneID : (double)startLatitude : (double)startLongitude {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    CLLocation *startLocation = [[CLLocation alloc] initWithLatitude:startLatitude longitude:startLongitude];
    NSArray *zone_id= [[appDelegate DB] displayTable:@"zone_info" columnName:@"zone_id"];
    NSArray *zoneTableData_lat= [[appDelegate DB] displayTable:@"zone_info" columnName:@"latitude"];
    NSArray *zoneTableData_longitude= [[appDelegate DB] displayTable:@"zone_info" columnName:@"longitude"];
    double min_distance = 1500;     //in metres
    double smallestDist = min_distance;
    NSLog(@"Entered getClosestZoneLocation_ZoneID : (lat,long)= (%f,%f)", startLatitude,startLongitude);
    NSLog(@"Entered getClosestZoneLocation_ZoneID LATS %@ %@", zoneTableData_lat, zoneTableData_longitude);
    for(int i=0; i<[zoneTableData_lat count]; i++ ) {
        double loc_x = [ zoneTableData_lat[i]  doubleValue];
        double loc_y = [ zoneTableData_longitude[i]  doubleValue];
        
        CLLocation *endLocation = [[CLLocation alloc] initWithLatitude:loc_x longitude:loc_y];
        CLLocationDistance distance = [startLocation distanceFromLocation:endLocation];
        NSLog(@"%@ - %@ = %f", startLocation, endLocation, distance);
        if(distance < smallestDist) {
            smallestDist = distance;
            //--Setting SharedPrefere
            #pragma mark SharedPreferences region
            [Utilities setRegionSharedPreference:zone_id[i]];
        }
    }
    if(smallestDist == min_distance){
        [Utilities setRegionSharedPreference:selectZoneID];
    }
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Cannot find the location.");
}

- (void) locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    NSLog(@"Started Monitoring for Region:\n%@",region.description);
    [manager requestStateForRegion:region];
}


-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    NSLog(@"Entered Region - %@", region.identifier);
    // post a notification
   NSString *regionName = [Utilities getRegionNameWithID:region.identifier];
   [Utilities showAlertWithTitle:@"Entering trillZone" andMessage:[NSString stringWithFormat:@"Entering %@", regionName]];
   [Utilities postNotification:@"Entering trillZone" withMessage:@"trillZone can fetch offers for you in this location"];

}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    NSLog(@"Exited Region - %@", region.identifier);
    NSString *regionName = [Utilities getRegionNameWithID:region.identifier];
    [Utilities showAlertWithTitle:@"Exiting trillZone" andMessage:[NSString stringWithFormat:@"Exiting %@", regionName]];
    // post a notification
    [Utilities postNotification:@"Exiting trillZone" withMessage:@"Exiting trillZone, enter for more offers"];
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(nullable CLRegion *)region withError:(nonnull NSError *)error:(CLRegion *)region
{
    NSLog(@"Monitor Failed for Region - %@", region.identifier);
    // post a notification
}

-(void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region{
    NSLog(@"self.locationManager didDetermineState");
    if (state == CLRegionStateInside) {
        //  if user already in the region, when monitoring is started
        NSString *regionName = [Utilities getRegionNameWithID:region.identifier];
//        [Utilities showAlertWithTitle:@"State inside Geofence" andMessage:[NSString stringWithFormat:@"You are in region with identifier %@", regionName]];
        #pragma mark SharedPreferences region
        [Utilities setRegionSharedPreference:region.identifier];
        [self loadPage];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)apiCalls {
    API_DATABASE_INFO = [NSString stringWithFormat: @"%@\/get_all_info\/?all=true",API_BASE_URL];
    API_LOGIN_REQUEST = [NSString stringWithFormat: @"%@\/login/",API_BASE_URL];
    API_VERIFY_OTP = [NSString stringWithFormat: @"%@\/verify\/otp/",API_BASE_URL];
    API_COUPON_INFO = [NSString stringWithFormat: @"%@\/coupons/?all=true",API_BASE_URL];
    API_ZONES_INFO = [NSString stringWithFormat: @"%@\/trillzone/",API_BASE_URL];
    API_SONGS_LIST = [NSString stringWithFormat: @"%@\/vote\/songs/",API_BASE_URL];
    API_USER_ACTIONS = [NSString stringWithFormat: @"%@\/trillzone",API_BASE_URL];
    API_BOOKMARK_COUPON = [NSString stringWithFormat: @"%@\/bookmark",API_BASE_URL];
}

//-----------------------------
#pragma mark - Loading Everything on top of ScrollView
//---------------

-(void) loadPage {
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];    //Clearing everything on screen
    //--
    self.myScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];   //Init scrollview
    //self.myScrollView.frame = CGRectMake(0, 0,CGRectGetWidth(self.view.frame),CGRectGetHeight(self.view.frame));

    //----vIDEO
    [self videoPlayback:@"record_start"];
    
    //----Adding Background Programatically
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
//    imageView.image =  [UIImage imageNamed:@"background_1920"];
//    [imageView setContentMode:UIViewContentModeScaleAspectFill];
//    [self.view addSubview:imageView];
    //---Tiles
    [self scroller];
    if(appDelegate.progressBarShow ==YES)
        [self progressBarCreation];
    [self.myScrollView addSubview:self.label];  //Animated text
    [self.view addSubview:self.myScrollView];   //Add scrollView on Top of imageView
}


-(void)scroller {
    //-----TB Logo
    CGFloat yAxisOfTBLogo = (self.view.frame.size.height*1/7);//self.view.frame.size.height*1/7 ;
    
    UIImage *img = [UIImage imageNamed:@"tbLogo_neon"];
    self.hotTile = [[UIImageView alloc] init];
    self.hotTile.image = img;
    self.hotTile.frame = CGRectMake(0,  yAxisOfTBLogo/2, self.view.frame.size.width, self.view.frame.size.height*1/5*0.9);
    self.hotTile.contentMode = UIViewContentModeCenter;
    self.hotTile.contentMode = UIViewContentModeScaleAspectFit;
    self.hotTile.center = CGPointMake(self.myScrollView.center.x, self.myScrollView.center.y - 1.2*self.navigationController.navigationBar.frame.size.height);
    //self.hotTile.center = self.view.center;
    
    //---Ripple
    [self rippleAnimate:hotTile];
    [self.myScrollView addSubview:hotTile]; //---addSubview , Add TB LOGO on top of ripple ANIMATION
    
    //-----Text HOT OFFERS
//    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(16, yAxisOfTBLogo*1.5 + hotTile.frame.size.height, self.view.frame.size.width, hotTile.frame.size.height/2)];
//    title.text = @"HOT OFFERS";
//    title.font = [UIFont fontWithName:@"Bebas" size:46];
//    title.textColor = [UIColor whiteColor];
//    [self.myScrollView addSubview:title];   //---addSubView HOTOFFERS
//    //---SubTitle NSAttributed string
//    UILabel *subtitle= [[UILabel alloc] initWithFrame:CGRectMake(16, title.frame.origin.y + title.frame.size.height, self.view.frame.size.width, title.frame.size.height/3)];
//    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"You are now connected By Sound!"];
//    //NSMAKERANGE takes start point and length
//    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-Light" size:36] range:NSMakeRange(0, 21)];
//    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-Bold" size:36] range:NSMakeRange(21, 10)];
//    [string addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,21)];
//    [string addAttribute:NSForegroundColorAttributeName value:[Utilities colorWithHexString:@"F5AB35"] range:NSMakeRange(21,10)];
//    subtitle.attributedText = string;
//    subtitle.numberOfLines = 0;
//    subtitle.minimumScaleFactor = 0.5;
//    subtitle.adjustsFontSizeToFitWidth = YES;
//    subtitle.lineBreakMode = NSLineBreakByTruncatingTail;
//
//    [self.myScrollView addSubview:subtitle]; //---addSubView
//
//    //-----For UIImages in scrollView
//    NSArray *myTitles = @[@"",@"", @"", @"CHECKI"];
//    CGFloat Titleopacity = 0.85;
//    NSArray *myImages = @[@"back_no_trill",@"back_no_trill",@"back_no_trill",@"back_no_trill"];
//    CGFloat imageWidth = self.view.frame.size.width - 32;
//    CGFloat imageHeight = self.view.frame.size.height*1/4;
//    CGFloat yPOS = self.view.frame.size.height*4/7 ;
//    CGFloat scrollViewContentSize = self.view.frame.size.height - imageHeight;                             //---ScrollView content size
//    self.myScrollView.contentSize = CGSizeMake(imageWidth,  self.view.frame.size.height+25);    //---ScrollView content size
//    //----Retrieval from Database
//    NSString *region = [[NSUserDefaults standardUserDefaults] objectForKey:@"region"];
//    NSString *condition = [NSString stringWithFormat:@"WHERE LOWER(type)=LOWER('hot') AND zone_id='%@'",region];
//    NSDictionary *tableData = [[appDelegate DB] displayTable:@"coupon_info" preCondition:nil selectfromTableNamePLUSCondition:condition];
//    NSMutableArray *unique_id = [[NSMutableArray alloc] init];
//    NSMutableArray *titles = [[NSMutableArray alloc] init];
//    NSMutableArray *image_URLs = [[NSMutableArray alloc] init];
//    for(int i=0; i<[tableData count]; i++){
//        NSString *key = [NSString stringWithFormat:@"%d",i];
//        NSDictionary *dict = [tableData valueForKey:key];
//        [unique_id addObject:[dict valueForKey:@"id"]];
//        [titles addObject:[dict valueForKey:@"brand_name"]];
//        [image_URLs addObject:[dict valueForKey:@"image"]];
//    }
//    NSDictionary *parameters = @{BOX_id: unique_id,
//                                 BOX_type: COUPON,
//
//                                 BOX_titles: titles,
//                                 BOX_imageURL: image_URLs,
//                                 BOX_titleOpacity: @(Titleopacity),
//                                 BOX_yPOS: @(yPOS),
//                                 BOX_scrollViewContentSize:@(scrollViewContentSize)
//                                 };
//    [Utilities makeRectangleBoxes:parameters withScrollView:self.myScrollView andViewController:self];
}


-(void)videoPlayback:(NSString*)name {
    [self.controller.view removeFromSuperview];
    [[self.view viewWithTag:2] removeFromSuperview];  //Remove Controller player
    [[self.view viewWithTag:1] removeFromSuperview];    //Remove button
    //[[self.view viewWithTag:0] removeFromSuperview];    //Remove button
    
//    NSURL *videoURL;
    int tag;
    tag=1;
//    videoURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"mp4_gif_start" ofType:@"mp4"]];
    if([name isEqualToString:@"record_start"]){
//        videoURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"mp4_gif_start" ofType:@"mp4"]];
        tag=1;
    }
    else if([name isEqualToString:@"record_stop"]){
//        videoURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"mp4_gif_stop" ofType:@"mp4"]];
        tag=0;
    }
    UIButton *getVC = [[UIButton alloc]initWithFrame:self.hotTile.frame];
    getVC.tag=tag; //Playing
    [getVC addTarget:self action:@selector(HomeButton:) forControlEvents:UIControlEventTouchDown];
    
    if([name isEqualToString:@"record_stop"]){
        [self.myScrollView addSubview:getVC];
        return;
    }
    //-----Animation
    NSString *myFilePath =  [[NSBundle mainBundle] pathForResource:name ofType:@"json"];
    NSData *myData = [NSData dataWithContentsOfFile:myFilePath];
    NSError *error = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:myData
                                                             options:kNilOptions
                                                               error:&error];
    LOTAnimationView *aview = [LOTAnimationView animationFromJSON:jsonDict];
    CGFloat sizeOfAnim = 300;
    aview.frame = CGRectMake( (self.view.frame.size.width-sizeOfAnim)/2, (self.view.frame.size.height)*0.3, sizeOfAnim , sizeOfAnim);
    aview.tag = 2;
    aview.center = self.hotTile.center;
    aview.contentMode = UIViewContentModeScaleAspectFit;
    aview.loopAnimation = YES;
    [self.myScrollView addSubview:aview];
    [aview play];
    //--
    [self.myScrollView addSubview:getVC];
    
//    self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(playerItemDidReachEnd:)
//                                                 name:AVPlayerItemDidPlayToEndTimeNotification
//                                               object:[self.player currentItem]];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:CMTimeMake(150, 100) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
}

-(void)HomeButton : (UIButton *)button{
    NSLog(@"Home Button tag : %d",button.tag);
    [self.player pause];
    if(button.tag==1){//PLAYING
        [[appDelegate tSDK] SDKdestructor];
        rippleLayer.hidden = NO;
        button.tag=0;
        text_array = @[@"TAP FOR DISCOUNTS",@"TAP TO GET REAL-TIME OFFERS",@"TAP TO START SENSING"];
        [self videoPlayback:@"record_stop"];
        //[self.myScrollView addSubview:self.label];
    } else if (button.tag==0) { //Not playing
        [[appDelegate tSDK] startRecording];
        rippleLayer.hidden = YES;
        button.tag= 1;
        text_array = @[@"SEARCHING BY SOUND",@"SENSING YOUR ENVIRONMENT",@"FINDING BEST OFFERS FOR YOU"];
        [self videoPlayback:@"record_start"];
        //[self.myScrollView addSubview:self.label];
    }
}

//------------------------------------
#pragma mark - Progress Bar
//------------------------------------

float time_gap=0.05;
int initSeconds=1;
int currMinute;
int currSeconds;
float totalSeconds;

int progress_array[3];  //Has time in minutes of the trill points with timer
short int progress_array_iterator = 0;

-(void)setCurrentTime:(int)minutes :(int)seconds{
    currMinute= minutes;
    currSeconds=seconds;
    totalSeconds = currMinute*60 + currSeconds;
}

-(void)progressBarCreation {
    //-----Progress View
    self.progressView = [[UIProgressView alloc]initWithFrame:CGRectMake(40, self.hotTile.frame.origin.y+self.hotTile.frame.size.height+90, self.view.frame.size.width-80, 50)];
    self.progressView.progressViewStyle = UIProgressViewStyleBar;
    self.progressView.transform = CGAffineTransformScale(self.progressView.transform, 1, 2);
    self.progressView.progressTintColor = [Utilities colorWithHexString:secondary];
    self.progressView.trackTintColor = [Utilities colorWithHexString:@"111111"];
    self.progressView.layer.cornerRadius = 8;
    self.progressView.clipsToBounds = true;
    [self.progressView setProgress:0 animated:NO];
    [self.myScrollView addSubview:self.progressView];       //----addSubview
    
    [self startCount];  //----TIMER
    
    //------Label
    CGFloat size = 50;
    self.labelProgress = [[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width-size)/2, self.progressView.frame.origin.y+self.progressView.frame.size.height+2, size, 25)];
    self.labelProgress.textColor = [Utilities colorWithHexString:black_two];
    self.labelProgress.text = @"";
    self.labelProgress.clearsContextBeforeDrawing = true;
    [self.myScrollView addSubview:self.labelProgress];       //----addSubview
    
    //------TEXT label
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, self.labelProgress.frame.origin.y+self.labelProgress.frame.size.height, self.view.frame.size.width, 25)];
    label.textColor = [Utilities colorWithHexString:black_two];
    label.text = @"to unlock exclusive offer";
    label.clearsContextBeforeDrawing = true;
    label.textAlignment = NSTextAlignmentCenter;
    [self.myScrollView addSubview:label];       //----addSubview
}

- (void)startCount{
    [self.myTimer invalidate];
    self.myTimer = nil;
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:time_gap target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
}

- (void)updateUI:(NSTimer *)timer
{
    static float count =0.0; count=count+(100/totalSeconds*time_gap);
    //NSLog(@"%f %f",(count/(100/totalSeconds*time_gap),floor(count/(100/totalSeconds*time_gap) )));
    //NSLog(@"In %f", count);
    if (count <100){
        float x = (float)count/100;
        [self.progressView setProgress:x animated:YES];
        if( (int)(count/(100/totalSeconds*time_gap) )%(int)(1/time_gap)==0  ){   //Detects when every 1 second passes
            [self timerFired];
        }
    } else {
        //--Add & SHOW trillPoints
        [Utilities addTrillPoints:(progress_array[progress_array_iterator]*10)];
        [Utilities initilizeKicks:(progress_array[progress_array_iterator]*10)];
        //--Reset
        count =0;
        [self.myTimer invalidate];
        self.myTimer = nil;
        progress_array_iterator++;
        //--Reload page
        if(progress_array_iterator<sizeof(progress_array)/sizeof(progress_array[0])){   //Check size of progress_array
            [self setCurrentTime:progress_array[progress_array_iterator] :01];
            [self loadPage];
        } else {
            progress_array_iterator = 0;
            appDelegate.progressBarShow = NO;
            [self loadPage];
        }//if else
    }//if count<100
}


-(void)timerFired {
    
    if((currMinute>0 || currSeconds>=0) && currMinute>=0)
    {
        if(currSeconds==0)
        {
            currMinute-=1;
            currSeconds=59;
        }
        else if(currSeconds>0)
        {
            currSeconds-=1;
        }
        if(currMinute>-1){
            NSLog(@"%@",[NSString stringWithFormat:@"%d:%02d",currMinute,currSeconds]);
            [self.labelProgress setText:[NSString stringWithFormat:@"%d:%02d",currMinute,currSeconds]];
        }
    }
}


-(void)rippleAnimate: (UIImageView *)tbLogoTile{
    CGPoint origin =  tbLogoTile.center;
    UIColor *color = [UIColor whiteColor];
    CGFloat startRadius = CGRectGetHeight(tbLogoTile.frame)/2 ;
    CGFloat endRadius = CGRectGetHeight(tbLogoTile.frame)/2 + 15;
    NSTimeInterval duration = 0.7f;
    
    rippleLayer = [CAShapeLayer layer];
    rippleLayer.hidden = YES;
    [rippleLayer setFillColor:[[UIColor whiteColor] CGColor]];
    rippleLayer.strokeColor = [[UIColor clearColor] CGColor];
    rippleLayer.opacity = 0.25;
    UIBezierPath *startPath = [UIBezierPath bezierPathWithArcCenter:origin
                                                             radius:startRadius
                                                         startAngle:0 endAngle:2*M_PI
                                                          clockwise:YES];
    UIBezierPath *endPath = [UIBezierPath bezierPathWithArcCenter:origin
                                                           radius:endRadius
                                                       startAngle:0 endAngle:(2*M_PI)
                                                        clockwise:YES];
    CABasicAnimation *rippleAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
    rippleAnimation.fromValue = (id)(startPath.CGPath);
    rippleAnimation.toValue = (id)(endPath.CGPath);
    rippleAnimation.duration = duration;
    rippleAnimation.repeatCount = HUGE_VALF;
    rippleAnimation.autoreverses = YES;
    
    [self.myScrollView.layer addSublayer:rippleLayer];
    [rippleLayer addAnimation:rippleAnimation forKey:nil];
    
    //--pulse animation effect
    //    CABasicAnimation *theAnimation;
    //    theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    //    theAnimation.duration=1.0;
    //    theAnimation.repeatCount=HUGE_VALF;
    //    theAnimation.autoreverses=YES;
    //    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    //    theAnimation.toValue=[NSNumber numberWithFloat:0.0];
    //    [hotTile.layer addAnimation:theAnimation forKey:@"animateOpacity"];
}



// MARK: ISHPullUpContentDelegate


-(void)pullUpViewController:(ISHPullUpViewController *)pullUpViewController updateEdgeInsets:(UIEdgeInsets)edgeInsets forContentViewController:(UIViewController *)contentVC
{
    self.view.layoutMargins = edgeInsets;
    [self.view layoutIfNeeded];
}

@end
