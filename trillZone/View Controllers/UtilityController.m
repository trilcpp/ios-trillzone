//
//  UtilityController.m
//  trillZone
//
//  Created by Shivansh on 2/2/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "UtilityController.h"

@interface UtilityController ()

@end

@implementation UtilityController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadPage];
 

}

-(void)viewDidAppear:(BOOL)animated{
    [self.tabBarController.tabBar setHidden:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    //---NavigationBar Colour set to blue
    [[self navigationController] navigationBar].barTintColor = [Utilities colorWithHexString:black_one];
}


-(void) loadPage {
    //---Adding background colour
    self.view.backgroundColor = [Utilities colorWithHexString:black_one];
    
    //----Adding Background Programatically
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    imageView.image =  [UIImage imageNamed:@"background_theatre"];
//    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    //---Tiles
    [self scroller];
    
    //[self.view addSubview:imageView];
    [self.view addSubview:self.myScrollView];   //Add scrollView on Top of imageView
    
}



-(void)scroller {
    
    CGFloat Titleopacity = 0.85;
    //----For UIImages in scrollView
    NSArray *myImages = @[@"search_store",@"mall_map"];//,@"back_no_trill"];
    CGFloat yPOS = 16 ;
    CGFloat scrollViewContentSize = yPOS*2;
    
    NSArray *myTitles = @[@"STORES",@"MALL MAP"];//, @"PARKING"];
    NSArray *unique_id = @[@"1",@"2"];//,@"3"];
    //NSArray *image_URLs = @[@"https://s3.ap-southeast-1.amazonaws.com/portaltrillbit/app_images/utility/stores.jpeg",
    //                        @"https://s3.ap-southeast-1.amazonaws.com/portaltrillbit/app_images/utility/Mall_Map.jpg"];
                            //@"https://s3.ap-southeast-1.amazonaws.com/portaltrillbit/app_images/utility/Parking.png"];
    
    NSDictionary *parameters = @{BOX_type: UTILITY,
                                 BOX_id: unique_id,
                                 //BOX_imageURL: image_URLs ,
                                 
                                 BOX_titles: myTitles,
                                 BOX_titleOpacity: @(Titleopacity),
                                 BOX_images: myImages,
                                 BOX_yPOS: @(yPOS),
                                 BOX_scrollViewContentSize:@(scrollViewContentSize)
                        };
    
    [Utilities makeRectangleBoxes:parameters withScrollView:self.myScrollView andViewController:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
