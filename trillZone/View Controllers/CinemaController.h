//
//  CinemaController.h
//  trillZone
//
//  Created by Shivansh on 2/1/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Utilities.h"

@interface CinemaController : UIViewController

@property (nonatomic, strong) AppDelegate *appDelegate;
@property (strong, nonatomic) UIScrollView *myScrollView;
@property (strong, nonatomic) UIActivityIndicatorView *indicator;

@end
