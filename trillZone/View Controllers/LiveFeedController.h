//
//  LiveFeedController.h
//  trillZone
//
//  Created by Shivansh on 2/9/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utilities.h"


@interface LiveFeedController : UIViewController <UIImagePickerControllerDelegate>
@property (strong, nonatomic) UIImageView *imageView;

@property(nonatomic, strong) UITextView *textview; //For posting message
@property (nonatomic,strong) UIScrollView *myScrollView;
@property (nonatomic, strong) AppDelegate *appDelegate; 
@end
