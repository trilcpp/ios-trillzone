//
//  BottomViewController.h
//  trillZone
//
//  Created by Mac Mini on 6/1/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iCarousel.h>

#import <ISHPullUpViewController.h>
#import <ISHPullUpHandleView.h>
#import <ISHPullUpRoundedView.h>

#import "Utilities.h"
#include "ImageFetcher.h"

@interface BottomViewController : UIViewController <iCarouselDataSource,iCarouselDelegate, ISHPullUpStateDelegate,ISHPullUpSizingDelegate>
@property (nonatomic, strong) AppDelegate *appDelegate;
//For BottomVC
@property (strong,nonatomic) UILabel *label;    //for brand_name
@property (strong,nonatomic) UITextView *textView; 
@property (strong, nonatomic) NSMutableArray *brand_title;
@property (strong, nonatomic) NSMutableArray *image_campaign;
@property (strong, nonatomic) NSMutableArray *descriptions;
@property (nonatomic) BOOL wrap;//for wrapping

@property (strong, nonatomic) ISHPullUpViewController* pullUpController;

@property (strong, nonatomic) ISHPullUpRoundedView *roundedView;
@property (strong, nonatomic) ISHPullUpHandleView *handleView;
@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UILabel *topLabel;
@property (strong, nonatomic) UIScrollView *myScrollView;

@property (strong, nonatomic) iCarousel *mycarousel;


@end
