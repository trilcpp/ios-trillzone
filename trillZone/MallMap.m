//
//  MallMap.m
//  trillZone
//
//  Created by Shivansh on 2/9/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "MallMap.h"

@interface MallMap ()
{
    CGFloat leaveTopHeight;
}
@end

@implementation MallMap
@synthesize appDelegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.tabBarController.tabBar setHidden:YES];
    self.navigationController.navigationBar.barTintColor = [Utilities colorWithHexString:black_one];
    self.navigationController.navigationBar.tintColor = [Utilities colorWithHexString:secondary];
    
    leaveTopHeight = 0;//self.navigationController.navigationBar.frame.size.height + 30;
    
    UIView *solidcolor = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, leaveTopHeight)];
    solidcolor.backgroundColor = [Utilities colorWithHexString:@"1e1e1e"];
    [self.view addSubview:solidcolor];
    
    //-----------CAPSPageMenu
    // Array to keep track of controllers in page menu
    NSMutableArray *controllerArray = [NSMutableArray array];
    // Create variables for all view controllers you want to put in the
    // page menu, initialize them, and add each to the controller array.
    // (Can be any UIViewController subclass)
    // Make sure the title property of all view controllers is set
    // Example:
    //    UIViewController *controller = [[UIViewController alloc] initWithNibName:@"LoginID" bundle:nil];
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSString *regionID = [[NSUserDefaults standardUserDefaults] objectForKey:@"region"];
    NSString *condition = [NSString stringWithFormat:@"WHERE zone_id='%@' ORDER BY F_Level ASC",regionID];
    NSDictionary *tableData = [[appDelegate DB]displayTable:@"zone_map" preCondition:nil selectfromTableNamePLUSCondition:condition];
    
    if([tableData count]==0){
        [Utilities showAlertWithTitle:@"MallMap" andMessage:@"Maps cannot be fetched for this mall currently"];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    for(int i=0; i<[tableData count];i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        NSString *floor_name = [dict objectForKey:@"floor_name"];
        NSString *url = [dict objectForKey:@"floor_image"];
        //---VC
        UIViewController *vc = [[UIViewController alloc]init];
        vc.title = floor_name ;
        [self mallMap_viewcontrollers:vc andURL:url];
        [controllerArray addObject:vc];
    }
    
    // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
    // Example:
    //----nOTE: Set segmented control as NO to get desired effect of lengthiness, i.e, wrapping font as horizontal scrolling
    NSDictionary *parameters = @{CAPSPageMenuOptionMenuItemSeparatorWidth: @(4.3),
                                 CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(NO),
                                 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                                 CAPSPageMenuOptionMenuItemWidth: @(125),
                                 
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [Utilities colorWithHexString:black_one],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [Utilities colorWithHexString:secondary],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [Utilities colorWithHexString:secondary],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"Raleway-Bold" size:14],
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor: [Utilities colorWithHexString:secondary]
                                 };
    // Initialize page menu with controller array, frame, and optional parameters
    self.pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, leaveTopHeight, self.view.frame.size.width, self.view.frame.size.height - leaveTopHeight) options:parameters];
    // Lastly add page menu as subview of base view controller view
    // or use pageMenu controller in you view hierachy as desired
    [self.view addSubview:_pageMenu.view];
    ///-----------CAPSPageMenu @end
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        [self.tabBarController.tabBar setHidden:NO];
    }
}

-(void)mallMap_viewcontrollers: (UIViewController *)VC andURL:(NSString *)mapURL{
    UIWebView *webView = [[UIWebView alloc] initWithFrame:VC.view.frame];
    //---Adding scrollView for PINCHZOOM
    UIScrollView *SV = [[UIScrollView alloc] initWithFrame:VC.view.frame];
    SV.userInteractionEnabled = YES;
    SV.minimumZoomScale=0.5;
    SV.maximumZoomScale=6.0;
    SV.contentSize=CGSizeMake(1280, 960);
    SV.delegate=self;
    [VC.view addSubview:SV]; //---addSubview
    //----WebView
    NSString *urlAddress = mapURL;
    NSURL *url = [[NSURL alloc] initWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
    [SV addSubview:webView];//--addSubview to view
}

@end
