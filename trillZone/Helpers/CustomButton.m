//
//  CustomButton.m
//  trillZone
//
//  Created by Shivansh on 2/7/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "CustomButton.h"


//Implementation file (.m)
@implementation UICustomButton


@synthesize unique_id, type, imageView;
@synthesize array;

- (id)initWithFrame:(CGRect)frame uniqueID:(NSString *)unique_id andType:(NSString *)type
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.type = type;
        self.unique_id = unique_id;
        
        UIImage *img = [UIImage imageNamed:@"back_no_trill"];
        [self setImage:img forState:UIControlStateHighlighted];
    }
    return self;
}


- (id)initCloseButtonWithFrame:(CGRect)frame andView:(UIView *)TOPview
{
    self = [super initWithFrame:frame];
    if (self) {
        self.TOPview = TOPview;
    }
    return self;
}

- (id)initFavButtonWithFrame:(CGRect)frame uniqueID:(NSString *)unique_id andImageView:(UIImageView *)imgview{
    self = [super initWithFrame:frame];
    if (self) {
        self.unique_id = unique_id;
        self.imageView = imgview;
    }
    return self;
}


- (void)buttonClose{
    
//     [self.TOPview.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
}


@end
