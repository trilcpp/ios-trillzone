//
//  ImageFetcher.h
//  trillZone
//
//  Created by Shivansh on 2/7/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIImageView+WebCache.h>
#import <SDWebImagePrefetcher.h>


@interface ImageFetcher : NSObject
+ (void)prefetchImages:(NSArray *)images;
+ (void)cardPopOver;

@end
