//
//  LoginController.m
//  trillZone
//
//  Created by Shivansh on 11/7/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import "LoginController.h"
#import "Utilities.h"

@interface LoginController ()

@end

@implementation LoginController

@synthesize appDelegate;
@synthesize signInButton;
@synthesize loginButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //----Adding background colour
    self.view.backgroundColor = [Utilities colorWithHexString:@"1e1e1e"];
    ;
    //----Google/Login
        [GIDSignIn sharedInstance].uiDelegate = self;
        //Uncomment to automatically sign in the user.
        //[[GIDSignIn sharedInstance] signInSilently];
//        signInButton.style = kGIDSignInButtonStyleStandard;
//        signInButton.colorScheme = kGIDSignInButtonColorSchemeLight;

    //----FaceBook/Login
    //    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    //        // Optional: Place the button in the center of your view.
    //    loginButton.center = self.view.center;
    //    [self.view addSubview:loginButton];
    
    // User is logged in, do work such as go to next view controller.
//    self.loginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    
}

//--Putting it in viewDidAppear makes it go through another check after 'Continue' is clicked and automatic sign in is there
-(void)viewDidAppear:(BOOL)animated {
    if ([FBSDKAccessToken currentAccessToken]) {
        [Utilities skipLoginController:NO];
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                NSLog(@"user:%@", result);
                [self sendToServer:result];
            }
        }];
    }
}

- (IBAction)FBbutton:(id)sender {
    //---GRAPH API
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    NSLog(@"press");
//    if ([UIApplication.sharedApplication canOpenURL:[NSURL URLWithString:@"fb://"]])
//    {
//        login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
//    }
    
    [login logInWithPublishPermissions:@[@"publish_actions"] fromViewController:self
                               handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
                                   if (error) {
                                       NSLog(@"Unexpected login error: %@", error);
                                       NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: @"There was a problem logging in. Please try again later.";
                                       NSString *alertTitle = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Oops";
                                       [[[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                                   }
                                   else {
                                       if(result.token)   // This means if There is current access token.
                                       {
                                           //-----Check publish permission
                                           if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
                                               // TODO: publish content.
                                               NSLog(@"LoginController.m: Inside Facebook current accesstoken");
                                               return;
                                           } else {
                                               NSLog(@"LoginController.m: Inside else");
                                               //----Logins again so I commented it out
//                                               FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
//                                               [loginManager logInWithPublishPermissions:@[@"publish_actions"]
//                                                                      fromViewController:self
//                                                                                 handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
//                                                                                     //TODO: process error or result.
//                                                                                     NSLog(@"LoginController.m: Inside Facebook loop");
//                                                                                 }];
                                           }
                                           
                                           [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                                               if (!error) {
                                                   NSLog(@"LoginController.m: Facebook data retrieved:%@", result);
                                                   [self sendToServer:result];
                                                   //Push ViewController
                                                   [Utilities skipLoginController:YES];
                                               }
                                               else{
                                                   NSLog(@"LoginController.m: Graph APi ERROR %@", [error localizedDescription]);
                                               }
                                           }];
                                       }
                                       NSLog(@"Login Cancel");
                                   }
                               }];

}

- (IBAction)Gbutton:(id)sender {
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] signIn];
}

- (IBAction)guestButton:(id)sender {

    //----Writing Shared Preferences
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:@"Guest" forKey:login_type];
    [preferences setObject:@"Guest" forKey:user_name];
    [preferences setObject:@"" forKey:user_email];
    [preferences setObject:@"" forKey:user_imageURL];
    const BOOL didSave = [preferences synchronize];
    if (!didSave)
        NSLog(@"\n--SDK_message : LoginController/sendToServer error : UserDefaults saving usertoken while writing");
    
    NSString *accessToken = [FBSDKAccessToken currentAccessToken].tokenString;
    //....Send to Server
    NSURL *signinEndpointURL = [NSURL URLWithString: [NSString stringWithFormat: @"%@\/login/",URLhit ]];
    
    NSDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:@"N/A" forKey:@"name"];
    [dict setValue:@"N/A" forKey:@"age_group"];
    [dict setValue:@"N/A" forKey:@"email"];
    [dict setValue:@"N/A" forKey:@"gender"];
    [dict setValue:@"N/A" forKey:@"auth"];
    [dict setValue:[appDelegate adId] forKey:@"device_id"];
    [dict setValue:@"guest" forKey:@"login_type"];
    [dict setValue:[appDelegate device_model] forKey:@"device_modal"];
    [dict setValue:@"N/A" forKey:@"social_id"];
    [dict setValue:@"N/A" forKey:@"phone_no"];
    [dict setValue:@"N/A" forKey:@"location"];
    [dict setValue:@"N/A" forKey:@"language"];
    [dict setValue:@"N/A" forKey:@"pro_pic"];
    
    NSLog(@"\Guest Login dictionary:\n %@",dict);
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:signinEndpointURL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:requestData];
    
    [Utilities sendRequestToServer:request message:@"Facebook Login" withResponseCallback:^(NSDictionary *responseJSON) {
        NSLog(@"\Guest ResponseJSON:\n%@",responseJSON);
        [LoginController WritingSharedPreferences:responseJSON];
        [appDelegate sendFCMtoServer:responseJSON[@"payload"][@"usertoken"]];
    }];
    [Utilities skipLoginController:YES];
}


//--------------
#pragma mark - Facebook Login
//--------------
-(void) sendToServer :(id)result{
    
    NSString *userId = [NSString stringWithFormat:@"%@", result[@"id"]];
    NSString *fullName = [NSString stringWithFormat:@"%@",result[@"name"]];
    NSString *email = [NSString stringWithFormat:@"%@",result[@"email"]];
    NSString *profile_picURL = [NSString stringWithFormat:@"%@",result[@"picture"][@"data"][@"url"]];
    
    //----Writing Shared Preferences
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:@"FB" forKey:login_type];
    [preferences setObject:fullName forKey:user_name];
    [preferences setObject:email forKey:user_email];
    [preferences setObject:profile_picURL forKey:user_imageURL];
    //---
    const BOOL didSave = [preferences synchronize];
    if (!didSave)
        NSLog(@"\n--SDK_message : LoginController/sendToServer error : UserDefaults saving usertoken while writing");
    
    NSString *accessToken = [FBSDKAccessToken currentAccessToken].tokenString;
    //....Send to Server
    NSURL *signinEndpointURL = [NSURL URLWithString: [NSString stringWithFormat: @"%@\/login/",URLhit ]];
    
    NSDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:fullName forKey:@"name"];
    [dict setValue:@"N/A" forKey:@"age_group"];
    [dict setValue:email forKey:@"email"];
    [dict setValue:@"N/A" forKey:@"gender"];
    [dict setValue:accessToken forKey:@"auth"];
    [dict setValue:[appDelegate adId] forKey:@"device_id"];
    [dict setValue:@"facebook" forKey:@"login_type"];
    [dict setValue:[appDelegate device_model] forKey:@"device_modal"];
    [dict setValue:userId forKey:@"social_id"];
    [dict setValue:@"N/A" forKey:@"phone_no"];
    [dict setValue:@"N/A" forKey:@"location"];
    [dict setValue:@"N/A" forKey:@"language"];
    [dict setValue:profile_picURL forKey:@"pro_pic"];
    
    NSLog(@"\nFacebook Login dictionary:\n %@",dict);
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:signinEndpointURL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:requestData];
    
    [Utilities sendRequestToServer:request message:@"Facebook Login" withResponseCallback:^(NSDictionary *responseJSON) {
        NSLog(@"\Facebook ResponseJSON:\n%@",responseJSON);
        [LoginController WritingSharedPreferences:responseJSON];
        [appDelegate sendFCMtoServer:responseJSON[@"payload"][@"usertoken"]];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//--------------
#pragma mark - Google Login
//--------------
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    //---Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSString *accessToken = user.authentication.accessToken;
    NSString *givenName = user.profile.givenName;
    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
    
    NSUInteger dimension = round(50 * [[UIScreen mainScreen] scale]);
    NSURL *imgURL = [user.profile imageURLWithDimension:dimension];
    NSString *imageURL = imgURL.absoluteString;
    
    //---If button is just tapped and no sign in happens
    if(email==nil) return;
    
    //----Writing Shared Preferences
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:fullName forKey:user_name];
    [preferences setObject:email forKey:user_email];
    [preferences setObject:imageURL forKey:user_imageURL];
    const BOOL didSave = [preferences synchronize];
    if (!didSave)
        NSLog(@"\n--SDK_message : LoginController/sendToServer error : UserDefaults saving usertoken while writing");
    
    
    //----Send to Server
    NSURL *signinEndpointURL = [NSURL URLWithString: [NSString stringWithFormat: @"%@\/login/",URLhit ]];
    NSDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:fullName forKey:@"name"];
    [dict setValue:@"N/A" forKey:@"age_group"];
    [dict setValue:email forKey:@"email"];
    [dict setValue:@"N/A" forKey:@"gender"];
    [dict setValue:accessToken forKey:@"auth"];
    [dict setValue:[appDelegate adId] forKey:@"device_id"];
    [dict setValue:@"google" forKey:@"login_type"];
    [dict setValue:[appDelegate device_model] forKey:@"device_modal"];
    [dict setValue:userId forKey:@"social_id"];
    [dict setValue:@"N/A" forKey:@"phone_no"];
    [dict setValue:@"N/A" forKey:@"location"];
    [dict setValue:@"N/A" forKey:@"language"];
    [dict setValue:@"N/A" forKey:@"pro_pic"];
    NSLog(@"\nGoogle Login, sending JSON :\n %@",dict);
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:signinEndpointURL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:requestData];
    
    [Utilities sendRequestToServer:request message:@"Appdelegate/Google Login" withResponseCallback:^(NSDictionary *responseJSON) {
        NSLog(@"\nGoogle ResponseJSON:\n%@",responseJSON);
        [LoginController WritingSharedPreferences:responseJSON];
        [appDelegate sendFCMtoServer:responseJSON[@"payload"][@"usertoken"]];
    }];
    [Utilities skipLoginController:YES];
}


//---------
#pragma mark - Shared Preference for Access Token
//--------

+(void) WritingSharedPreferences : (NSDictionary*) jsondata {
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSDictionary *payload = [jsondata objectForKey:@"payload"];
    NSString *usertoken = [NSString stringWithFormat:@"%@", [payload objectForKey:@"usertoken"]];    //1-user_token
    [preferences setObject:usertoken forKey:@"usertoken"];
    
    
    int trill_point = [[payload valueForKey:trill_points]integerValue];  //2-trill_points
    int total_points = [[payload valueForKey:@"points_available"]integerValue];
    if(trill_point!=0){
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self trillPointCard:trill_point andViewController:[appDelegate window].rootViewController];
            });
        [preferences setInteger:(total_points+trill_point) forKey:trill_points];
    }else{
        [preferences setInteger:(total_points) forKey:trill_points];
    }
    
    //  Save to disk
    const BOOL didSave = [preferences synchronize];
    if (!didSave)
        NSLog(@"\n--SDK_message : Login.m error : UserDefaults saving usertoken while writing");
}

+(BOOL) check_SharedPreferences {
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *usertoken = [preferences objectForKey:@"usertoken"];
    if (usertoken == nil ) return false;
    else return true;
}

+(void)trillPointCard:(int)points andViewController:(UIViewController *)current_vc{
    //current_vc = current_vc.tabBarController;
    //---Making View
    UIView *popOverCoupon = [[UIView alloc] initWithFrame:current_vc.view.frame];
    UIImageView *opaqueImage = [[UIImageView alloc] initWithFrame:popOverCoupon.frame];
    opaqueImage.layer.backgroundColor = [[UIColor blackColor]CGColor];
    opaqueImage.layer.opacity = 0.75;
    [popOverCoupon addSubview:opaqueImage];//--Add blackCover on popOverView
    UICustomButton *close_popover =[[UICustomButton alloc] initCloseButtonWithFrame:popOverCoupon.frame andView:popOverCoupon];
    close_popover.vc = current_vc;
    [close_popover addTarget:[Utilities class] action:@selector(closeButtonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [popOverCoupon addSubview:close_popover]; //----Add close BUTTON on popOverView
    
    //---CardView
    UIImageView *card = [[UIImageView alloc] initWithFrame:CGRectMake(20, popOverCoupon.frame.size.height/7, popOverCoupon.frame.size.width-40, popOverCoupon.frame.size.height*5/7)];
    card.backgroundColor = [Utilities colorWithHexString:@"1e1e1e"];
    
    //-Image on card
    NSString *image_URL = @"https://s3-ap-southeast-1.amazonaws.com/portaltrillbit/app_images/trillpoints.jpg";
    UIImageView *card_image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, card.frame.size.width, card.frame.size.height)];
    [card_image sd_setImageWithURL:[NSURL URLWithString:image_URL] placeholderImage:[UIImage imageNamed:@"back_no_trill"]];
    //card_image.image = [UIImage imageNamed:@"trillpoints"];
    card_image.contentMode = UIViewContentModeCenter;
    card_image.contentMode = UIViewContentModeScaleAspectFill;
    card_image.clipsToBounds = YES;
    [card addSubview:card_image];
    //-Text TITLE
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(8, card_image.frame.size.height + 4, card_image.frame.size.width - 16, 25)];
    title.text = [NSString stringWithFormat:@"+%d TRILL POINTS",points];
    title.font = [UIFont fontWithName:@"Raleway-Bold" size:25];
    title.textColor = [Utilities colorWithHexString:secondary];
    title.textAlignment = NSTextAlignmentCenter;
    title.minimumScaleFactor = 0.5;
    title.adjustsFontSizeToFitWidth = YES;
    [card addSubview:title];
    //*********** Dynamic SIZE
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(8, card_image.frame.size.height +title.frame.size.height+ 4, card_image.frame.size.width - 16, 25.0f)];
    // add text to the UITextView first so we know what size to make it
    NSString *string = @"i. Check Trill Points in 'Account' Section\nii. Min 300 points to redeem\niii. Redeemable at our partner stores";
    textView.font = [UIFont fontWithName:@"Raleway-Light" size:12];
    textView.text = string;
    textView.textAlignment = NSTextAlignmentLeft;
    textView.textColor = [UIColor whiteColor];
    textView.backgroundColor = [UIColor clearColor];
    // get the size of the UITextView based on what it would be with the text
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    [card addSubview:textView];     //--addSubview on card
    //********************
    //---Changing card frame
    CGFloat newCardHeight = card.frame.size.height +(newFrame.size.height+title.frame.size.height) + 10;
    card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y, card.frame.size.width, newCardHeight);
    card.center = popOverCoupon.center;
    //-Shadow for card
    UIImageView *shadowView = [[UIImageView alloc] initWithFrame:card.frame];
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
    shadowView.layer.masksToBounds = NO;
    shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    shadowView.layer.shadowOffset = CGSizeMake(2.0f, 3.0f);
    shadowView.layer.shadowOpacity = 0.9f;
    shadowView.layer.shadowPath = shadowPath.CGPath;
    shadowView.layer.shadowRadius = 1.0;
    [popOverCoupon addSubview:shadowView];//--Add shadow on popOverView
    //
    
    [popOverCoupon addSubview:card]; //--Add card on popOverView
    
    //---Making cross button for closing
    UIImageView *close = [[UIImageView alloc] initWithFrame:CGRectMake( card.frame.size.width + card.frame.origin.x -40, card.frame.origin.y, 40, 40)];
    close.layer.shadowColor = [[UIColor blackColor]CGColor];
    close.layer.masksToBounds = NO;
    close.layer.shadowColor = [UIColor blackColor].CGColor;
    close.layer.shadowOffset = CGSizeMake(1.2f, 1.2f);
    close.layer.shadowOpacity = 0.35f;
    close.layer.shadowRadius = 1.0;
    //close.backgroundColor = [Utilities colorWithHexString:@"1b9fc6"];
    close.layer.opacity = 0.95f;
    close.image = [UIImage imageNamed:@"cross_white"];
    [popOverCoupon addSubview:close]; //--Add close VIEW on popOverView
    //-Adding CLOSE button
    UICustomButton *close_button =[[UICustomButton alloc] initCloseButtonWithFrame:close.frame andView:popOverCoupon];
    close_button.vc = current_vc;
    
    [close_button addTarget:[Utilities class] action:@selector(closeButtonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [popOverCoupon addSubview:close_button]; //----Add close BUTTON on popOverView
    //END
    
    [popOverCoupon setUserInteractionEnabled:YES];
    [current_vc.view addSubview:popOverCoupon];
}


@end
