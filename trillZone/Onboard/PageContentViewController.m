//
//  PageContentViewController.m
//  trillZone
//
//  Created by Shivansh on 1/17/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "PageContentViewController.h"

@interface PageContentViewController ()

@end

@implementation PageContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.image.image = [UIImage imageNamed:self.imageFile];
//    self.titleLabel.font = [UIFont fontWithName:@"Raleway-Bold" size:17];
    self.titleLabel.text = self.titleText;
//    self.paragraph.font = [UIFont fontWithName:@"Raleway-Bold" size:15];
    
    
    //----Conditions for background
    switch(self.pageIndex){
        case 0: [self setUpDescription:self.pageIndex : @"212121"];
            break;
        case 1: [self setUpDescription:self.pageIndex : @"feeee1"];
            self.titleLabel.textColor = [Utilities colorWithHexString:@"212121"];
            self.paragraph.textColor = [Utilities colorWithHexString:@"212121"];
            break;
        case 2: [self setUpDescription:self.pageIndex : @"ffffff"];
            self.titleLabel.textColor = [Utilities colorWithHexString:@"212121"];
            self.paragraph.textColor = [Utilities colorWithHexString:@"212121"];
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setUpDescription:(NSInteger)index :(NSString *)color{
    self.view.backgroundColor = [Utilities colorWithHexString:color];
    switch(index){
        case 0: self.paragraph.text = @"Welcome.\nYour device has unlocked a new feature! You can now recieve data without any network, using Sound.";
            break;
        case 1: self.paragraph.text = @"Go for a shopping spree in a trillZone!\nGet the latest offers from your favorite stores. Redeem them for great discounts!";
            break;
        case 2: self.paragraph.text = @"Simply walk-in stores to for earning trill points and redeem them to get big discounts on any product!";
            break;
    }
}


@end



//----------------------------------
#pragma mark - UIPageView baseClass
//----------------------------------
@implementation BasePageViewController

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    UIScrollView *scrollView = nil;
    UIPageControl *pageControl = nil;
    
    for(UIView *view in self.view.subviews)
    {
        if([view isKindOfClass:[UIScrollView class]])
        {
            scrollView = (UIScrollView *)view;
        }else if([view isKindOfClass:[UIPageControl class]])
        {
            pageControl = (UIPageControl *)view;
        }
    }
    
    if(scrollView != nil && pageControl!=nil)
    {
        scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view bringSubviewToFront:pageControl];
    }
}

@end
