//
//  PageContentViewController.h
//  trillZone
//
//  Created by Shivansh on 1/17/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utilities.h"

@interface PageContentViewController : UIViewController <UIPageViewControllerDataSource>

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UITextView *paragraph;


@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;

@end


//For the UIPageViewController
@interface BasePageViewController : UIPageViewController

@end
