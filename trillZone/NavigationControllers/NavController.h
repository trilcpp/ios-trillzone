//
//  NavController.h
//  trillZone
//
//  Created by Shivansh on 1/31/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlaces/GooglePlaces.h>
#import <QuartzCore/QuartzCore.h> //--Added for [button 'layer'] property , in leftbarbuttonItem

#import "Utilities.h"
#import "SWRevealView.h"
#import "HomeController.h"  //---For its closestLocation function

@interface NavController : UINavigationController <UITableViewDelegate, UITableViewDataSource, GMSAutocompleteTableDataSourceDelegate, UISearchControllerDelegate>
{
    int btnMenuFlag;
}
@property (nonatomic, strong) AppDelegate *appDelegate; //Global array declaration
//---For GooglePlaces
@property (nonatomic,strong) GMSPlacesClient *placesClient;  //---GooglePlacesAPI
@property (nonatomic,strong) UITextField *textField; //---For searching location
//---For Table on Searching Location
@property (nonatomic,strong) UIView *dropDown;
@property (nonatomic,strong) UIScrollView *searchDropDown;

@end
